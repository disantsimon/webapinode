/*! *****************************************************************************
    Copyright (c) Di Sante Simone. All rights reserved.
    
    This file is part of lib webapinode.

    webapinode is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    webapinode is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    For GNU General Public License see <http://www.gnu.org/licenses/>.
*/
import { Application } from 'express';
export declare class WebApiServer {
    private app;
    private mapped;
    routes: {};
    constructor();
    /**
     * return all routes registered for this webapiserver
     */
    getRoutes(): any;
    /**
     * this method will start all http request setted in your controller.
     * Controller should be an object class
     *
     * @param controllers array of objects used as controller; if not null only the given objects will be used as controllers,
     * else will be used all classes decorated as @Controller
     */
    start(controllers?: Array<any>): WebApiServer;
    private getCallType;
    private getCallName;
    /**
     * return the instance of the given controller
     * @param controller name or type of controller that you would get
     */
    getController<T>(controller: any): T;
    /**
     * you could call this to start the server on a port. You could also call method getExpressInstance().listen(port);
     * @param port value that refer to the port of the server
     */
    listen(port: any): WebApiServer;
    /**
     * return the express instance to make some operation like
     */
    getExpressInstance(): Application;
}
