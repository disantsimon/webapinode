/*! *****************************************************************************
    Copyright (c) Di Sante Simone. All rights reserved.
    
    This file is part of lib webapinode.

    webapinode is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    webapinode is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    For GNU General Public License see <http://www.gnu.org/licenses/>.
*/
export declare class fnObject {
    fn: any;
    middlewares: any;
    props: {
        route: string;
        propertyName: string;
        parsed: boolean;
    };
}
export declare enum callType {
    GET = 0,
    POST = 1,
    PUT = 2,
    DELETE = 3,
    PATCH = 4,
    HEAD = 5,
    COPY = 6,
    LINK = 7,
    UNLINK = 8,
    PURGE = 9,
    PROPFIND = 10,
    LOCK = 11,
    UNLOCK = 12
}
