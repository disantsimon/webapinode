/*! *****************************************************************************
    Copyright (c) Di Sante Simone. All rights reserved.
    
    This file is part of lib webapinode.

    webapinode is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    webapinode is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    For GNU General Public License see <http://www.gnu.org/licenses/>.
*/
export declare var controllers: {};
/**
 * This decorator set a class as controller, so each function decorated with a Http decorator function will be a route for your api
 *
 * 1) As you can see Controller accepts one argument (ex "user"), if given that will be the name of your controller. If you define a function get()
 * that could be reached, if decorated with HttpGET(), with a simple GET request as user. If argument name is not given, then the http will start
 * with class name, example class UserController{} then name will simply user
 *
 * @Controller() //1)
 * class MyController{
 *
 * }
 *
 * another example
 *
 * in this case each http request start with just us/, because "us" is the given name by decorator
 * @Controller("us")
 * class User{}
 *
 * @param name give a name to the class(controller)
 */
export declare function Controller(name?: any): (target: any) => void;
/**
 * that set a Controller function as a simple Http GET request
 * @param route name of the Http GET request, if null will be automatically replaced by function name if that name is different to get
 * @param middlewares all middleware functions for the request
 */
export declare function HttpGET(route?: string, middlewares?: Array<(request?: any, response?: any, next?: any) => void>): (target: any, propertyName: any) => void;
/**
 * that set a Controller function as a simple Http POST request
 * @param route name of the Http POST request, if null will be automatically replaced by function name if that name is different to post
 * @param middlewares all middleware functions for the request
 */
export declare function HttpPOST(route?: string, middlewares?: Array<(request?: any, response?: any, next?: any) => void>): (target: any, propertyName: any) => void;
/**
 * that set a Controller function as a simple Http PUT request
 * @param route name of the Http PUT request, if null will be automatically replaced by function name if that name is different to put
 * @param middlewares all middleware functions for the request
 */
export declare function HttpPUT(route?: string, middlewares?: Array<(request?: any, response?: any, next?: any) => void>): (target: any, propertyName: any) => void;
/**
 * that set a Controller function as a simple Http DELETE request
 * @param route name of the Http DELETE request, if null will be automatically replaced by function name if that name is different to delete
 * @param middlewares all middleware functions for the request
 */
export declare function HttpDELETE(route?: string, middlewares?: Array<(request?: any, response?: any, next?: any) => void>): (target: any, propertyName: any) => void;
/**
 * that set a Controller function as a simple Http PATCH request
 * @param route name of the Http PATCH request, if null will be automatically replaced by function name if that name is different to patch
 * @param middlewares all middleware functions for the request
 */
export declare function HttpPATCH(route?: string, middlewares?: Array<(request?: any, response?: any, next?: any) => void>): (target: any, propertyName: any) => void;
/**
 * that set a Controller function as a simple Http HEAD request
 * @param route name of the Http HEAD request, if null will be automatically replaced by function name if that name is different to head
 * @param middlewares all middleware functions for the request
 */
export declare function HttpHEAD(route?: string, middlewares?: Array<(request?: any, response?: any, next?: any) => void>): (target: any, propertyName: any) => void;
/**
 * that set a Controller function as a simple Http COPY request
 * @param route name of the Http COPY request, if null will be automatically replaced by function name if that name is different to copy
 * @param middlewares all middleware functions for the request
 */
export declare function HttpCOPY(route?: string, middlewares?: Array<(request?: any, response?: any, next?: any) => void>): (target: any, propertyName: any) => void;
/**
 * that set a Controller function as a simple Http LINK request
 * @param route name of the Http LINK request, if null will be automatically replaced by function name if that name is different to link
 * @param middlewares all middleware functions for the request
 */
export declare function HttpLINK(route?: string, middlewares?: Array<(request?: any, response?: any, next?: any) => void>): (target: any, propertyName: any) => void;
/**
 * that set a Controller function as a simple Http UNLINK request
 * @param route name of the Http UNLINK request, if null will be automatically replaced by function name if that name is different to unlink
 * @param middlewares all middleware functions for the request
 */
export declare function HttpUNLINK(route?: string, middlewares?: Array<(request?: any, response?: any, next?: any) => void>): (target: any, propertyName: any) => void;
/**
 * that set a Controller function as a simple Http PURGE request
 * @param route name of the Http PURGE request, if null will be automatically replaced by function name if that name is different to purge
 * @param middlewares all middleware functions for the request
 */
export declare function HttpPURGE(route?: string, middlewares?: Array<(request?: any, response?: any, next?: any) => void>): (target: any, propertyName: any) => void;
/**
 * that set a Controller function as a simple Http PROPFIND request
 * @param route name of the Http PROPFIND request, if null will be automatically replaced by function name if that name is different to propfind
 * @param middlewares all middleware functions for the request
 */
export declare function HttpPROPFIND(route?: string, middlewares?: Array<(request?: any, response?: any, next?: any) => void>): (target: any, propertyName: any) => void;
