@ Controller()

class UserController{

    @HttpGET()
    get(request,response,next){
    //do something, url for GET request "user/"
    }

    @HttpGET("info")
    getUserInfo(request,response,next){
    //do something, url for GET request "user/info"
    }

    @HttpPOST()
    post(){
    //do something, url for POST request "user/"
    }

    @HttpPOST()
    addSomeUser(){
    //do something, url for POST request "user/addsomeuser"
    }

    @HttpPOST("setrule")
    assignUserRole(){
    //do something, url for POST request "user/setrule"
    }
}  

@ Controller("dep/first")

class FirstDepartmentController{
    /**
    ** in this case each request could be the same as 
    ** UserController, but each url will be precede by dep/first/ 
    **
}   


##usage
import { WebApiServer } from 'webapimodel'

import { Product } from './controllers/product';

declare var require;

require("./Controllers")

/**
    * create a webApiServer and start listen on port
    */
    
let srv1 = new WebApiServer().listen(3020);

let srv2 = new WebApiServer().listen(3021);

/**
    * each class controller 
    */
    
srv1.start();

/**
    * get the express instance created
    */
    
var appSrv2 = srv1.getExpressInstance();

appSrv2.listen(3099, (err: string) => {

    if (err) {
        return console.log('something bad happened', err)
    }
    console.log(`server is listening on ${3099}`)
})

/**
    * give a specific controller instance
    */
    
var controllerProduct = srv1.getController<Product>(Product);

//var controllerProduct = srv1.getController<Product>("Product"); will give the same result

srv2.getExpressInstance().use(function (request, response, next) {

    response.locals.server = 3021;
    next()
})

/**
    * only given controller will start
    */
    
srv2.start([Product]);                        

// for a full sample

git clone https://disantsimon@bitbucket.org/disantsimon/webapinodesample.git 

npm install

node app.js