/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*! *****************************************************************************
    Copyright (c) Di Sante Simone. All rights reserved.
    
    This file is part of lib webapinode.

    webapinode is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    webapinode is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    For GNU General Public License see <http://www.gnu.org/licenses/>.
*/
/** @type {?} */
var controllers = {};
/**
 * This decorator set a class as controller, so each function decorated with a Http decorator function will be a route for your api
 *
 * 1) As you can see Controller accepts one argument (ex "user"), if given that will be the name of your controller. If you define a function get()
 * that could be reached, if decorated with HttpGET(), with a simple GET request as user. If argument name is not given, then the http will start
 * with class name, example class UserController{} then name will simply user
 *
 * \@Controller() //1)
 * class MyController{
 *
 * }
 *
 * another example
 *
 * in this case each http request start with just us/, because "us" is the given name by decorator
 * \@Controller("us")
 * class User{}
 *
 * @param {?=} name give a name to the class(controller)
 * @return {?}
 */
function Controller(name) {
    return function Controller(target) {
        name = name || "";
        /** @type {?} */
        var opt = {
            value: name.trim ? name.trim() : name,
            enumerable: false,
            writable: false,
            configurable: false
        };
        controllers[target.name] = new target;
        Object.defineProperty(controllers[target.name], "__routeprefix", opt);
    };
}
/**
 * @param {?} fns
 * @param {?} target
 * @param {?} propertyName
 * @param {?} middlewares
 * @param {?} route
 * @return {?}
 */
function addFunction(fns, target, propertyName, middlewares, route) {
    fns.push({
        fn: target[propertyName],
        middlewares: middlewares,
        props: { route: route, propertyName: propertyName }
    });
}
/**
 * @param {?} target
 * @param {?} fn
 * @return {?}
 */
function init(target, fn) {
    if (Object.getOwnPropertyDescriptor(target, fn) == undefined) {
        Object.defineProperty(target, fn, {
            value: [],
            enumerable: false,
            writable: false
        });
    }
}
// * 
//  * @Controller
//  * class UserController{
//  * 
//  *  @HttpGET()
//  *  get(request,response,next){
//  *      //request GET to user  
//  *  }  
//  * 
//  *  @HttpGET()
//  *  getByName(request,response,next){
//  *      //request GET to user/getbyname  
//  *  }  
//  * 
//  *  @HttpGET("status")
//  *  getStatus(request,response,next){
//  *      //request GET to user/status  
//  *  }  
//  * 
//  * }
//  *  
/**
 * that set a Controller function as a simple Http GET request
 * @param {?=} route name of the Http GET request, if null will be automatically replaced by function name if that name is different to get
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
function HttpGET(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnGet");
        addFunction(target.__fnGet, target, propertyName, middlewares, route);
    };
}
/**
 * that set a Controller function as a simple Http POST request
 * @param {?=} route name of the Http POST request, if null will be automatically replaced by function name if that name is different to post
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
function HttpPOST(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnPost");
        addFunction(target.__fnPost, target, propertyName, middlewares, route);
    };
}
/**
 * that set a Controller function as a simple Http PUT request
 * @param {?=} route name of the Http PUT request, if null will be automatically replaced by function name if that name is different to put
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
function HttpPUT(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnPut");
        addFunction(target.__fnPut, target, propertyName, middlewares, route);
    };
}
/**
 * that set a Controller function as a simple Http DELETE request
 * @param {?=} route name of the Http DELETE request, if null will be automatically replaced by function name if that name is different to delete
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
function HttpDELETE(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnDelete");
        addFunction(target.__fnDelete, target, propertyName, middlewares, route);
    };
}
/**
 * that set a Controller function as a simple Http PATCH request
 * @param {?=} route name of the Http PATCH request, if null will be automatically replaced by function name if that name is different to patch
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
function HttpPATCH(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnPatch");
        addFunction(target.__fnPatch, target, propertyName, middlewares, route);
    };
}
/**
 * that set a Controller function as a simple Http HEAD request
 * @param {?=} route name of the Http HEAD request, if null will be automatically replaced by function name if that name is different to head
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
function HttpHEAD(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnHead");
        addFunction(target.__fnHead, target, propertyName, middlewares, route);
    };
}
/**
 * that set a Controller function as a simple Http COPY request
 * @param {?=} route name of the Http COPY request, if null will be automatically replaced by function name if that name is different to copy
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
function HttpCOPY(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnCopy");
        addFunction(target.__fnCopy, target, propertyName, middlewares, route);
    };
}
/**
 * that set a Controller function as a simple Http LINK request
 * @param {?=} route name of the Http LINK request, if null will be automatically replaced by function name if that name is different to link
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
function HttpLINK(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnLink");
        addFunction(target.__fnLink, target, propertyName, middlewares, route);
    };
}
/**
 * that set a Controller function as a simple Http UNLINK request
 * @param {?=} route name of the Http UNLINK request, if null will be automatically replaced by function name if that name is different to unlink
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
function HttpUNLINK(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnUnlink");
        addFunction(target.__fnUnlink, target, propertyName, middlewares, route);
    };
}
/**
 * that set a Controller function as a simple Http PURGE request
 * @param {?=} route name of the Http PURGE request, if null will be automatically replaced by function name if that name is different to purge
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
function HttpPURGE(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnPurge");
        addFunction(target.__fnPurge, target, propertyName, middlewares, route);
    };
}
/**
 * that set a Controller function as a simple Http PROPFIND request
 * @param {?=} route name of the Http PROPFIND request, if null will be automatically replaced by function name if that name is different to propfind
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
function HttpPROPFIND(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnPropfind");
        addFunction(target.__fnPropfind, target, propertyName, middlewares, route);
    };
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {number} */
const callType = {
    GET: 0,
    POST: 1,
    PUT: 2,
    DELETE: 3,
    PATCH: 4,
    HEAD: 5,
    COPY: 6,
    LINK: 7,
    UNLINK: 8,
    PURGE: 9,
    PROPFIND: 10,
    LOCK: 11,
    UNLOCK: 12,
};
callType[callType.GET] = 'GET';
callType[callType.POST] = 'POST';
callType[callType.PUT] = 'PUT';
callType[callType.DELETE] = 'DELETE';
callType[callType.PATCH] = 'PATCH';
callType[callType.HEAD] = 'HEAD';
callType[callType.COPY] = 'COPY';
callType[callType.LINK] = 'LINK';
callType[callType.UNLINK] = 'UNLINK';
callType[callType.PURGE] = 'PURGE';
callType[callType.PROPFIND] = 'PROPFIND';
callType[callType.LOCK] = 'LOCK';
callType[callType.UNLOCK] = 'UNLOCK';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*! *****************************************************************************
    Copyright (c) Di Sante Simone. All rights reserved.
    
    This file is part of lib webapinode.

    webapinode is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    webapinode is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    For GNU General Public License see <http://www.gnu.org/licenses/>.
*/
/**
 * @param {?} _route
 * @param {?} target
 * @param {?} property
 * @param {?} prefix
 * @param {?} call
 * @return {?}
 */
function parseRoute(_route, target, property, prefix, call) {
    /** @type {?} */
    let route = _route.props.route;
    if (_route.props.parsed)
        return route;
    /** @type {?} */
    let hasPrefix = prefix != null && prefix != "";
    /** @type {?} */
    let hasRoute = _route.props.route != "" && _route.props.route != null;
    /** @type {?} */
    let controllerClassName = target.constructor.name.toLowerCase().replace("controller", "");
    // console.log("route " + route, "hasroute " + hasRoute, "hasPrefix=>" + prefix, "controllerClassName=>" + controllerClassName);
    if (!hasRoute) {
        if (property == call) {
            route = hasPrefix ? prefix : controllerClassName;
        }
        else {
            route = hasPrefix ? prefix : controllerClassName;
            route += "/" + property;
        }
    }
    else {
        route = hasPrefix ? (prefix + "/" + _route.props.route) : _route.props.route;
    }
    _route.props.parsed = true;
    // console.log("parsed=>", route);
    return route;
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
var express = require('express');
class WebApiServer {
    constructor() {
        this.app = express();
        this.mapped = {};
        this.routes = {};
    }
    /**
     * return all routes registered for this webapiserver
     * @return {?}
     */
    getRoutes() {
        /** @type {?} */
        let res = [];
        for (var route in this.routes) {
            res.push(route);
        }
        return res;
    }
    /**
     * this method will start all http request setted in your controller.
     * Controller should be an object class
     *
     * @param {?=} controllers array of objects used as controller; if not null only the given objects will be used as controllers,
     * else will be used all classes decorated as \@Controller
     * @return {?}
     */
    start(controllers$$1) {
        /** @type {?} */
        let self = this;
        /** @type {?} */
        let _controllers = [];
        if (!controllers$$1) {
            for (let ctrl in controllers) {
                _controllers.push(controllers[ctrl]);
            }
        }
        else {
            _controllers = controllers$$1;
        }
        /**
         * @param {?} fn
         * @param {?} _callType
         * @param {?} ctrl
         * @return {?}
         */
        function setRoute(fn, _callType, ctrl) {
            if (!fn)
                return;
            for (let _fn of fn) {
                /** @type {?} */
                let prefix = (controllers[ctrl.constructor.name].__routeprefix);
                /** @type {?} */
                let call = self.getCallName(_callType);
                _fn.props.route = parseRoute(_fn, ctrl, _fn.props.propertyName, prefix, call);
                self.routes[call + "//" + _fn.props.route] = true;
                self.app[call]("/" + _fn.props.route, ...(_fn.middlewares || []).map(f => f.bind(ctrl)).concat([_fn.fn.bind(ctrl)]));
            }
        }
        for (let controller of _controllers) {
            /** @type {?} */
            var isConstructor = controller.name != null;
            if (isConstructor) {
                controller = new controller();
            }
            if (this.mapped[controller.constructor.name]) {
                continue;
            }
            this.mapped[controller.constructor.name] = true;
            /** @type {?} */
            let arrFn = [
                "__fnGet",
                "__fnPost",
                "__fnPut",
                "__fnDelete",
                "__fnPatch",
                "__fnHead",
                "__fnCopy",
                "__fnLink",
                "__fnUnlink",
                "__fnPurge",
                "__fnPropfind",
                "__fnLock",
                "__fnUnlock"
            ];
            for (let _fn of arrFn) {
                /** @type {?} */
                let ctrl = controllers[controller.constructor.name] || controller;
                /** @type {?} */
                let fn = ctrl[_fn];
                setRoute(fn, this.getCallType(_fn), ctrl);
            }
        }
        return this;
    }
    /**
     * @private
     * @param {?} typename
     * @return {?}
     */
    getCallType(typename) {
        return callType[typename.replace("__fn", "").toUpperCase()];
    }
    /**
     * @private
     * @param {?} type
     * @return {?}
     */
    getCallName(type) {
        return callType[type].toLowerCase();
    }
    /**
     * return the instance of the given controller
     * @template T
     * @param {?} controller name or type of controller that you would get
     * @return {?}
     */
    getController(controller) {
        if (controller)
            return controllers[controller] || controllers[controller.name];
        return (/** @type {?} */ (null));
    }
    /**
     * you could call this to start the server on a port. You could also call method getExpressInstance().listen(port);
     * @param {?} port value that refer to the port of the server
     * @return {?}
     */
    listen(port) {
        /** @type {?} */
        let self = this;
        self.app.listen(port, (err) => {
            if (err) {
                return console.log('something bad happened', err);
            }
            console.log(`server is listening on ${port}`);
        });
        return this;
    }
    /**
     * return the express instance to make some operation like
     * @return {?}
     */
    getExpressInstance() {
        return this.app;
    }
}

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { Controller, HttpGET, HttpPOST, HttpPUT, HttpDELETE, HttpPATCH, HttpHEAD, HttpCOPY, HttpLINK, HttpUNLINK, HttpPURGE, HttpPROPFIND, controllers, WebApiServer };

//# sourceMappingURL=webapimodel.js.map