/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*! *****************************************************************************
    Copyright (c) Di Sante Simone. All rights reserved.
    
    This file is part of lib webapinode.

    webapinode is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    webapinode is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    For GNU General Public License see <http://www.gnu.org/licenses/>.
*/
/*
 * Public API Surface of webapimodel asdf
 */
export { Controller, HttpGET, HttpPOST, HttpPUT, HttpDELETE, HttpPATCH, HttpHEAD, HttpCOPY, HttpLINK, HttpUNLINK, HttpPURGE, HttpPROPFIND, controllers } from './lib/decorators';
export { WebApiServer } from './lib/webapi';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljX2FwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3dlYmFwaW1vZGVsLyIsInNvdXJjZXMiOlsicHVibGljX2FwaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFzQkEsOEpBQWMsa0JBQWtCLENBQUE7QUFDaEMsNkJBQWMsY0FBYyxDQUFBIiwic291cmNlc0NvbnRlbnQiOlsiIFxuLyohICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXG4gICAgQ29weXJpZ2h0IChjKSBEaSBTYW50ZSBTaW1vbmUuIEFsbCByaWdodHMgcmVzZXJ2ZWQuXG4gICAgXG4gICAgVGhpcyBmaWxlIGlzIHBhcnQgb2YgbGliIHdlYmFwaW5vZGUuXG5cbiAgICB3ZWJhcGlub2RlIGlzIGZyZWUgc29mdHdhcmU6IHlvdSBjYW4gcmVkaXN0cmlidXRlIGl0IGFuZC9vciBtb2RpZnlcbiAgICBpdCB1bmRlciB0aGUgdGVybXMgb2YgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIGFzIHB1Ymxpc2hlZCBieVxuICAgIHRoZSBGcmVlIFNvZnR3YXJlIEZvdW5kYXRpb24sIGVpdGhlciB2ZXJzaW9uIDMgb2YgdGhlIExpY2Vuc2UsIG9yXG4gICAgKGF0IHlvdXIgb3B0aW9uKSBhbnkgbGF0ZXIgdmVyc2lvbi5cblxuICAgIHdlYmFwaW5vZGUgaXMgZGlzdHJpYnV0ZWQgaW4gdGhlIGhvcGUgdGhhdCBpdCB3aWxsIGJlIHVzZWZ1bCxcbiAgICBidXQgV0lUSE9VVCBBTlkgV0FSUkFOVFk7IHdpdGhvdXQgZXZlbiB0aGUgaW1wbGllZCB3YXJyYW50eSBvZlxuICAgIE1FUkNIQU5UQUJJTElUWSBvciBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRS4gIFNlZSB0aGVcbiAgICBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBmb3IgbW9yZSBkZXRhaWxzLlxuXG4gICAgRm9yIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIHNlZSA8aHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzLz4uXG4qL1xuXG4vKlxuICogUHVibGljIEFQSSBTdXJmYWNlIG9mIHdlYmFwaW1vZGVsIGFzZGZcbiAqL1xuZXhwb3J0ICogZnJvbSAnLi9saWIvZGVjb3JhdG9ycydcbmV4cG9ydCAqIGZyb20gJy4vbGliL3dlYmFwaSciXX0=