/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*! *****************************************************************************
    Copyright (c) Di Sante Simone. All rights reserved.
    
    This file is part of lib webapinode.

    webapinode is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    webapinode is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    For GNU General Public License see <http://www.gnu.org/licenses/>.
*/
export class fnObject {
    constructor() {
        this.props = { route: "", propertyName: "", parsed: false };
    }
}
if (false) {
    /** @type {?} */
    fnObject.prototype.fn;
    /** @type {?} */
    fnObject.prototype.middlewares;
    /** @type {?} */
    fnObject.prototype.props;
}
/** @enum {number} */
const callType = {
    GET: 0,
    POST: 1,
    PUT: 2,
    DELETE: 3,
    PATCH: 4,
    HEAD: 5,
    COPY: 6,
    LINK: 7,
    UNLINK: 8,
    PURGE: 9,
    PROPFIND: 10,
    LOCK: 11,
    UNLOCK: 12,
};
export { callType };
callType[callType.GET] = 'GET';
callType[callType.POST] = 'POST';
callType[callType.PUT] = 'PUT';
callType[callType.DELETE] = 'DELETE';
callType[callType.PATCH] = 'PATCH';
callType[callType.HEAD] = 'HEAD';
callType[callType.COPY] = 'COPY';
callType[callType.LINK] = 'LINK';
callType[callType.UNLINK] = 'UNLINK';
callType[callType.PURGE] = 'PURGE';
callType[callType.PROPFIND] = 'PROPFIND';
callType[callType.LOCK] = 'LOCK';
callType[callType.UNLOCK] = 'UNLOCK';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kZWxzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vd2ViYXBpbW9kZWwvIiwic291cmNlcyI6WyJsaWIvbW9kZWxzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxNQUFNLE9BQU8sUUFBUTtJQUFyQjtRQUdJLFVBQUssR0FBNkQsRUFBRSxLQUFLLEVBQUUsRUFBRSxFQUFFLFlBQVksRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxDQUFBO0lBQ3BILENBQUM7Q0FBQTs7O0lBSEcsc0JBQUc7O0lBQ0gsK0JBQVk7O0lBQ1oseUJBQWdIOzs7O0lBSWhILE1BQUc7SUFDSCxPQUFJO0lBQ0osTUFBRztJQUNILFNBQU07SUFDTixRQUFLO0lBQ0wsT0FBSTtJQUNKLE9BQUk7SUFDSixPQUFJO0lBQ0osU0FBTTtJQUNOLFFBQUs7SUFDTCxZQUFRO0lBQ1IsUUFBSTtJQUNKLFVBQU0iLCJzb3VyY2VzQ29udGVudCI6WyIgXHJcbi8qISAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG4gICAgQ29weXJpZ2h0IChjKSBEaSBTYW50ZSBTaW1vbmUuIEFsbCByaWdodHMgcmVzZXJ2ZWQuXHJcbiAgICBcclxuICAgIFRoaXMgZmlsZSBpcyBwYXJ0IG9mIGxpYiB3ZWJhcGlub2RlLlxyXG5cclxuICAgIHdlYmFwaW5vZGUgaXMgZnJlZSBzb2Z0d2FyZTogeW91IGNhbiByZWRpc3RyaWJ1dGUgaXQgYW5kL29yIG1vZGlmeVxyXG4gICAgaXQgdW5kZXIgdGhlIHRlcm1zIG9mIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBhcyBwdWJsaXNoZWQgYnlcclxuICAgIHRoZSBGcmVlIFNvZnR3YXJlIEZvdW5kYXRpb24sIGVpdGhlciB2ZXJzaW9uIDMgb2YgdGhlIExpY2Vuc2UsIG9yXHJcbiAgICAoYXQgeW91ciBvcHRpb24pIGFueSBsYXRlciB2ZXJzaW9uLlxyXG5cclxuICAgIHdlYmFwaW5vZGUgaXMgZGlzdHJpYnV0ZWQgaW4gdGhlIGhvcGUgdGhhdCBpdCB3aWxsIGJlIHVzZWZ1bCxcclxuICAgIGJ1dCBXSVRIT1VUIEFOWSBXQVJSQU5UWTsgd2l0aG91dCBldmVuIHRoZSBpbXBsaWVkIHdhcnJhbnR5IG9mXHJcbiAgICBNRVJDSEFOVEFCSUxJVFkgb3IgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UuICBTZWUgdGhlXHJcbiAgICBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBmb3IgbW9yZSBkZXRhaWxzLlxyXG5cclxuICAgIEZvciBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBzZWUgPGh0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy8+LlxyXG4qL1xyXG5cclxuZXhwb3J0IGNsYXNzIGZuT2JqZWN0IHtcclxuICAgIGZuO1xyXG4gICAgbWlkZGxld2FyZXM7XHJcbiAgICBwcm9wczogeyByb3V0ZTogc3RyaW5nLCBwcm9wZXJ0eU5hbWU6IHN0cmluZywgcGFyc2VkOiBib29sZWFuIH0gPSB7IHJvdXRlOiBcIlwiLCBwcm9wZXJ0eU5hbWU6IFwiXCIsIHBhcnNlZDogZmFsc2UgfVxyXG59XHJcblxyXG5leHBvcnQgZW51bSBjYWxsVHlwZSB7XHJcbiAgICBHRVQsXHJcbiAgICBQT1NULFxyXG4gICAgUFVULFxyXG4gICAgREVMRVRFLFxyXG4gICAgUEFUQ0gsXHJcbiAgICBIRUFELFxyXG4gICAgQ09QWSxcclxuICAgIExJTkssXHJcbiAgICBVTkxJTkssXHJcbiAgICBQVVJHRSxcclxuICAgIFBST1BGSU5ELFxyXG4gICAgTE9DSyxcclxuICAgIFVOTE9DS1xyXG59Il19