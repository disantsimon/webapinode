/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*! *****************************************************************************
    Copyright (c) Di Sante Simone. All rights reserved.
    
    This file is part of lib webapinode.

    webapinode is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    webapinode is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    For GNU General Public License see <http://www.gnu.org/licenses/>.
*/
import { controllers as Ctrls } from './decorators';
import { callType } from './models';
import { parseRoute } from './support';
/** @type {?} */
var express = require('express');
export class WebApiServer {
    constructor() {
        this.app = express();
        this.mapped = {};
        this.routes = {};
    }
    /**
     * return all routes registered for this webapiserver
     * @return {?}
     */
    getRoutes() {
        /** @type {?} */
        let res = [];
        for (var route in this.routes) {
            res.push(route);
        }
        return res;
    }
    /**
     * this method will start all http request setted in your controller.
     * Controller should be an object class
     *
     * @param {?=} controllers array of objects used as controller; if not null only the given objects will be used as controllers,
     * else will be used all classes decorated as \@Controller
     * @return {?}
     */
    start(controllers) {
        /** @type {?} */
        let self = this;
        /** @type {?} */
        let _controllers = [];
        if (!controllers) {
            for (let ctrl in Ctrls) {
                _controllers.push(Ctrls[ctrl]);
            }
        }
        else {
            _controllers = controllers;
        }
        /**
         * @param {?} fn
         * @param {?} _callType
         * @param {?} ctrl
         * @return {?}
         */
        function setRoute(fn, _callType, ctrl) {
            if (!fn)
                return;
            for (let _fn of fn) {
                /** @type {?} */
                let prefix = (Ctrls[ctrl.constructor.name].__routeprefix);
                /** @type {?} */
                let call = self.getCallName(_callType);
                _fn.props.route = parseRoute(_fn, ctrl, _fn.props.propertyName, prefix, call);
                self.routes[call + "//" + _fn.props.route] = true;
                self.app[call]("/" + _fn.props.route, ...(_fn.middlewares || []).map(f => f.bind(ctrl)).concat([_fn.fn.bind(ctrl)]));
            }
        }
        for (let controller of _controllers) {
            /** @type {?} */
            var isConstructor = controller.name != null;
            if (isConstructor) {
                controller = new controller();
            }
            if (this.mapped[controller.constructor.name]) {
                continue;
            }
            this.mapped[controller.constructor.name] = true;
            /** @type {?} */
            let arrFn = [
                "__fnGet",
                "__fnPost",
                "__fnPut",
                "__fnDelete",
                "__fnPatch",
                "__fnHead",
                "__fnCopy",
                "__fnLink",
                "__fnUnlink",
                "__fnPurge",
                "__fnPropfind",
                "__fnLock",
                "__fnUnlock"
            ];
            for (let _fn of arrFn) {
                /** @type {?} */
                let ctrl = Ctrls[controller.constructor.name] || controller;
                /** @type {?} */
                let fn = ctrl[_fn];
                setRoute(fn, this.getCallType(_fn), ctrl);
            }
        }
        return this;
    }
    /**
     * @private
     * @param {?} typename
     * @return {?}
     */
    getCallType(typename) {
        return callType[typename.replace("__fn", "").toUpperCase()];
    }
    /**
     * @private
     * @param {?} type
     * @return {?}
     */
    getCallName(type) {
        return callType[type].toLowerCase();
    }
    /**
     * return the instance of the given controller
     * @template T
     * @param {?} controller name or type of controller that you would get
     * @return {?}
     */
    getController(controller) {
        if (controller)
            return Ctrls[controller] || Ctrls[controller.name];
        return (/** @type {?} */ (null));
    }
    /**
     * you could call this to start the server on a port. You could also call method getExpressInstance().listen(port);
     * @param {?} port value that refer to the port of the server
     * @return {?}
     */
    listen(port) {
        /** @type {?} */
        let self = this;
        self.app.listen(port, (err) => {
            if (err) {
                return console.log('something bad happened', err);
            }
            console.log(`server is listening on ${port}`);
        });
        return this;
    }
    /**
     * return the express instance to make some operation like
     * @return {?}
     */
    getExpressInstance() {
        return this.app;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    WebApiServer.prototype.app;
    /**
     * @type {?}
     * @private
     */
    WebApiServer.prototype.mapped;
    /** @type {?} */
    WebApiServer.prototype.routes;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2ViYXBpLmpzIiwic291cmNlUm9vdCI6Im5nOi8vd2ViYXBpbW9kZWwvIiwic291cmNlcyI6WyJsaWIvd2ViYXBpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsV0FBVyxJQUFJLEtBQUssRUFBRSxNQUFNLGNBQWMsQ0FBQTtBQUNuRCxPQUFPLEVBQVksUUFBUSxFQUFFLE1BQU0sVUFBVSxDQUFDO0FBQzlDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxXQUFXLENBQUM7O0lBSW5DLE9BQU8sR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDO0FBR2hDLE1BQU0sT0FBTyxZQUFZO0lBUXJCO1FBTlEsUUFBRyxHQUFnQixPQUFPLEVBQUUsQ0FBQztRQUU3QixXQUFNLEdBQUcsRUFBRSxDQUFDO1FBRXBCLFdBQU0sR0FBRyxFQUFFLENBQUM7SUFFSSxDQUFDOzs7OztJQUtqQixTQUFTOztZQUNELEdBQUcsR0FBUSxFQUFFO1FBQ2pCLEtBQUssSUFBSSxLQUFLLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUMzQixHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ25CO1FBQ0QsT0FBTyxHQUFHLENBQUM7SUFDZixDQUFDOzs7Ozs7Ozs7SUFTTSxLQUFLLENBQUMsV0FBd0I7O1lBQzdCLElBQUksR0FBRyxJQUFJOztZQUVYLFlBQVksR0FBZSxFQUFFO1FBQ2pDLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDZCxLQUFLLElBQUksSUFBSSxJQUFJLEtBQUssRUFBRTtnQkFDcEIsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzthQUNsQztTQUNKO2FBQ0k7WUFDRCxZQUFZLEdBQUcsV0FBVyxDQUFDO1NBQzlCOzs7Ozs7O1FBQ0QsU0FBUyxRQUFRLENBQUMsRUFBYyxFQUFFLFNBQW1CLEVBQUUsSUFBSTtZQUN2RCxJQUFJLENBQUMsRUFBRTtnQkFBRSxPQUFPO1lBQ2hCLEtBQUssSUFBSSxHQUFHLElBQUksRUFBRSxFQUFFOztvQkFDWixNQUFNLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxhQUFhLENBQUM7O29CQUNyRCxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUM7Z0JBQ3RDLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLEdBQUcsQ0FBQyxLQUFLLENBQUMsWUFBWSxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztnQkFDOUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEdBQUcsSUFBSSxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDO2dCQUNsRCxJQUFJLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsR0FBRyxDQUFDLFdBQVcsSUFBSSxFQUFFLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDeEg7UUFDTCxDQUFDO1FBRUQsS0FBSyxJQUFJLFVBQVUsSUFBSSxZQUFZLEVBQUU7O2dCQUU3QixhQUFhLEdBQUcsVUFBVSxDQUFDLElBQUksSUFBSSxJQUFJO1lBQzNDLElBQUksYUFBYSxFQUFFO2dCQUNmLFVBQVUsR0FBRyxJQUFJLFVBQVUsRUFBRSxDQUFDO2FBQ2pDO1lBRUQsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzFDLFNBQVM7YUFDWjtZQUVELElBQUksQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxJQUFJLENBQUM7O2dCQUU1QyxLQUFLLEdBQUc7Z0JBQ1IsU0FBUztnQkFDVCxVQUFVO2dCQUNWLFNBQVM7Z0JBQ1QsWUFBWTtnQkFDWixXQUFXO2dCQUNYLFVBQVU7Z0JBQ1YsVUFBVTtnQkFDVixVQUFVO2dCQUNWLFlBQVk7Z0JBQ1osV0FBVztnQkFDWCxjQUFjO2dCQUNkLFVBQVU7Z0JBQ1YsWUFBWTthQUNmO1lBQ0QsS0FBSyxJQUFJLEdBQUcsSUFBSSxLQUFLLEVBQUU7O29CQUNmLElBQUksR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxVQUFVOztvQkFDdkQsRUFBRSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUM7Z0JBRWxCLFFBQVEsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQzthQUM3QztTQUNKO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQzs7Ozs7O0lBRU8sV0FBVyxDQUFDLFFBQWdCO1FBQ2hDLE9BQU8sUUFBUSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7SUFDaEUsQ0FBQzs7Ozs7O0lBRU8sV0FBVyxDQUFDLElBQWM7UUFDOUIsT0FBTyxRQUFRLENBQUMsSUFBSSxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDeEMsQ0FBQzs7Ozs7OztJQU1NLGFBQWEsQ0FBSSxVQUFVO1FBQzlCLElBQUksVUFBVTtZQUNWLE9BQU8sS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFdkQsT0FBTyxtQkFBSyxJQUFJLEVBQUEsQ0FBQztJQUNyQixDQUFDOzs7Ozs7SUFNTSxNQUFNLENBQUMsSUFBSTs7WUFDVixJQUFJLEdBQUcsSUFBSTtRQUNmLElBQUksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDLEdBQVcsRUFBRSxFQUFFO1lBQ2xDLElBQUksR0FBRyxFQUFFO2dCQUNMLE9BQU8sT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsRUFBRSxHQUFHLENBQUMsQ0FBQTthQUNwRDtZQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsMEJBQTBCLElBQUksRUFBRSxDQUFDLENBQUE7UUFDakQsQ0FBQyxDQUFDLENBQUE7UUFDRixPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDOzs7OztJQUtNLGtCQUFrQjtRQUNyQixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUM7SUFDcEIsQ0FBQztDQUNKOzs7Ozs7SUEvSEcsMkJBQXFDOzs7OztJQUVyQyw4QkFBb0I7O0lBRXBCLDhCQUFZIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbi8qISAqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKlxyXG4gICAgQ29weXJpZ2h0IChjKSBEaSBTYW50ZSBTaW1vbmUuIEFsbCByaWdodHMgcmVzZXJ2ZWQuXHJcbiAgICBcclxuICAgIFRoaXMgZmlsZSBpcyBwYXJ0IG9mIGxpYiB3ZWJhcGlub2RlLlxyXG5cclxuICAgIHdlYmFwaW5vZGUgaXMgZnJlZSBzb2Z0d2FyZTogeW91IGNhbiByZWRpc3RyaWJ1dGUgaXQgYW5kL29yIG1vZGlmeVxyXG4gICAgaXQgdW5kZXIgdGhlIHRlcm1zIG9mIHRoZSBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBhcyBwdWJsaXNoZWQgYnlcclxuICAgIHRoZSBGcmVlIFNvZnR3YXJlIEZvdW5kYXRpb24sIGVpdGhlciB2ZXJzaW9uIDMgb2YgdGhlIExpY2Vuc2UsIG9yXHJcbiAgICAoYXQgeW91ciBvcHRpb24pIGFueSBsYXRlciB2ZXJzaW9uLlxyXG5cclxuICAgIHdlYmFwaW5vZGUgaXMgZGlzdHJpYnV0ZWQgaW4gdGhlIGhvcGUgdGhhdCBpdCB3aWxsIGJlIHVzZWZ1bCxcclxuICAgIGJ1dCBXSVRIT1VUIEFOWSBXQVJSQU5UWTsgd2l0aG91dCBldmVuIHRoZSBpbXBsaWVkIHdhcnJhbnR5IG9mXHJcbiAgICBNRVJDSEFOVEFCSUxJVFkgb3IgRklUTkVTUyBGT1IgQSBQQVJUSUNVTEFSIFBVUlBPU0UuICBTZWUgdGhlXHJcbiAgICBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBmb3IgbW9yZSBkZXRhaWxzLlxyXG5cclxuICAgIEZvciBHTlUgR2VuZXJhbCBQdWJsaWMgTGljZW5zZSBzZWUgPGh0dHA6Ly93d3cuZ251Lm9yZy9saWNlbnNlcy8+LlxyXG4qL1xyXG5cclxuaW1wb3J0IHsgY29udHJvbGxlcnMgYXMgQ3RybHMgfSBmcm9tICcuL2RlY29yYXRvcnMnXHJcbmltcG9ydCB7IGZuT2JqZWN0LCBjYWxsVHlwZSB9IGZyb20gJy4vbW9kZWxzJztcclxuaW1wb3J0IHsgcGFyc2VSb3V0ZSB9IGZyb20gJy4vc3VwcG9ydCc7XHJcbmltcG9ydCB7IEFwcGxpY2F0aW9uIH0gZnJvbSAnZXhwcmVzcydcclxuZGVjbGFyZSB2YXIgcmVxdWlyZTogYW55XHJcblxyXG52YXIgZXhwcmVzcyA9IHJlcXVpcmUoJ2V4cHJlc3MnKTtcclxuXHJcblxyXG5leHBvcnQgY2xhc3MgV2ViQXBpU2VydmVyIHtcclxuXHJcbiAgICBwcml2YXRlIGFwcDogQXBwbGljYXRpb24gPSBleHByZXNzKCk7XHJcblxyXG4gICAgcHJpdmF0ZSBtYXBwZWQgPSB7fTtcclxuXHJcbiAgICByb3V0ZXMgPSB7fTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHsgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogcmV0dXJuIGFsbCByb3V0ZXMgcmVnaXN0ZXJlZCBmb3IgdGhpcyB3ZWJhcGlzZXJ2ZXJcclxuICAgICAqL1xyXG4gICAgZ2V0Um91dGVzKCkge1xyXG4gICAgICAgIGxldCByZXM6IGFueSA9IFtdO1xyXG4gICAgICAgIGZvciAodmFyIHJvdXRlIGluIHRoaXMucm91dGVzKSB7XHJcbiAgICAgICAgICAgIHJlcy5wdXNoKHJvdXRlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJlcztcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIHRoaXMgbWV0aG9kIHdpbGwgc3RhcnQgYWxsIGh0dHAgcmVxdWVzdCBzZXR0ZWQgaW4geW91ciBjb250cm9sbGVyLiBcclxuICAgICAqIENvbnRyb2xsZXIgc2hvdWxkIGJlIGFuIG9iamVjdCBjbGFzc1xyXG4gICAgICogXHJcbiAgICAgKiBAcGFyYW0gY29udHJvbGxlcnMgYXJyYXkgb2Ygb2JqZWN0cyB1c2VkIGFzIGNvbnRyb2xsZXI7IGlmIG5vdCBudWxsIG9ubHkgdGhlIGdpdmVuIG9iamVjdHMgd2lsbCBiZSB1c2VkIGFzIGNvbnRyb2xsZXJzLFxyXG4gICAgICogZWxzZSB3aWxsIGJlIHVzZWQgYWxsIGNsYXNzZXMgZGVjb3JhdGVkIGFzIEBDb250cm9sbGVyIFxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgc3RhcnQoY29udHJvbGxlcnM/OiBBcnJheTxhbnk+KTogV2ViQXBpU2VydmVyIHtcclxuICAgICAgICBsZXQgc2VsZiA9IHRoaXM7XHJcblxyXG4gICAgICAgIGxldCBfY29udHJvbGxlcnM6IEFycmF5PGFueT4gPSBbXTtcclxuICAgICAgICBpZiAoIWNvbnRyb2xsZXJzKSB7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGN0cmwgaW4gQ3RybHMpIHtcclxuICAgICAgICAgICAgICAgIF9jb250cm9sbGVycy5wdXNoKEN0cmxzW2N0cmxdKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBlbHNlIHtcclxuICAgICAgICAgICAgX2NvbnRyb2xsZXJzID0gY29udHJvbGxlcnM7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZ1bmN0aW9uIHNldFJvdXRlKGZuOiBmbk9iamVjdFtdLCBfY2FsbFR5cGU6IGNhbGxUeXBlLCBjdHJsKSB7XHJcbiAgICAgICAgICAgIGlmICghZm4pIHJldHVybjtcclxuICAgICAgICAgICAgZm9yIChsZXQgX2ZuIG9mIGZuKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgcHJlZml4ID0gKEN0cmxzW2N0cmwuY29uc3RydWN0b3IubmFtZV0uX19yb3V0ZXByZWZpeCk7XHJcbiAgICAgICAgICAgICAgICBsZXQgY2FsbCA9IHNlbGYuZ2V0Q2FsbE5hbWUoX2NhbGxUeXBlKTtcclxuICAgICAgICAgICAgICAgIF9mbi5wcm9wcy5yb3V0ZSA9IHBhcnNlUm91dGUoX2ZuLCBjdHJsLCBfZm4ucHJvcHMucHJvcGVydHlOYW1lLCBwcmVmaXgsIGNhbGwpO1xyXG4gICAgICAgICAgICAgICAgc2VsZi5yb3V0ZXNbY2FsbCArIFwiLy9cIiArIF9mbi5wcm9wcy5yb3V0ZV0gPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgc2VsZi5hcHBbY2FsbF0oXCIvXCIgKyBfZm4ucHJvcHMucm91dGUsIC4uLihfZm4ubWlkZGxld2FyZXMgfHwgW10pLm1hcChmID0+IGYuYmluZChjdHJsKSkuY29uY2F0KFtfZm4uZm4uYmluZChjdHJsKV0pKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZm9yIChsZXQgY29udHJvbGxlciBvZiBfY29udHJvbGxlcnMpIHtcclxuXHJcbiAgICAgICAgICAgIHZhciBpc0NvbnN0cnVjdG9yID0gY29udHJvbGxlci5uYW1lICE9IG51bGw7XHJcbiAgICAgICAgICAgIGlmIChpc0NvbnN0cnVjdG9yKSB7XHJcbiAgICAgICAgICAgICAgICBjb250cm9sbGVyID0gbmV3IGNvbnRyb2xsZXIoKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMubWFwcGVkW2NvbnRyb2xsZXIuY29uc3RydWN0b3IubmFtZV0pIHtcclxuICAgICAgICAgICAgICAgIGNvbnRpbnVlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLm1hcHBlZFtjb250cm9sbGVyLmNvbnN0cnVjdG9yLm5hbWVdID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgICAgIGxldCBhcnJGbiA9IFtcclxuICAgICAgICAgICAgICAgIFwiX19mbkdldFwiLFxyXG4gICAgICAgICAgICAgICAgXCJfX2ZuUG9zdFwiLFxyXG4gICAgICAgICAgICAgICAgXCJfX2ZuUHV0XCIsXHJcbiAgICAgICAgICAgICAgICBcIl9fZm5EZWxldGVcIixcclxuICAgICAgICAgICAgICAgIFwiX19mblBhdGNoXCIsXHJcbiAgICAgICAgICAgICAgICBcIl9fZm5IZWFkXCIsXHJcbiAgICAgICAgICAgICAgICBcIl9fZm5Db3B5XCIsXHJcbiAgICAgICAgICAgICAgICBcIl9fZm5MaW5rXCIsXHJcbiAgICAgICAgICAgICAgICBcIl9fZm5VbmxpbmtcIixcclxuICAgICAgICAgICAgICAgIFwiX19mblB1cmdlXCIsXHJcbiAgICAgICAgICAgICAgICBcIl9fZm5Qcm9wZmluZFwiLFxyXG4gICAgICAgICAgICAgICAgXCJfX2ZuTG9ja1wiLFxyXG4gICAgICAgICAgICAgICAgXCJfX2ZuVW5sb2NrXCJcclxuICAgICAgICAgICAgXVxyXG4gICAgICAgICAgICBmb3IgKGxldCBfZm4gb2YgYXJyRm4pIHtcclxuICAgICAgICAgICAgICAgIGxldCBjdHJsID0gQ3RybHNbY29udHJvbGxlci5jb25zdHJ1Y3Rvci5uYW1lXSB8fCBjb250cm9sbGVyO1xyXG4gICAgICAgICAgICAgICAgbGV0IGZuID0gY3RybFtfZm5dO1xyXG5cclxuICAgICAgICAgICAgICAgIHNldFJvdXRlKGZuLCB0aGlzLmdldENhbGxUeXBlKF9mbiksIGN0cmwpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0Q2FsbFR5cGUodHlwZW5hbWU6IHN0cmluZyk6IGNhbGxUeXBlIHtcclxuICAgICAgICByZXR1cm4gY2FsbFR5cGVbdHlwZW5hbWUucmVwbGFjZShcIl9fZm5cIiwgXCJcIikudG9VcHBlckNhc2UoKV07XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXRDYWxsTmFtZSh0eXBlOiBjYWxsVHlwZSk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIGNhbGxUeXBlW3R5cGVdLnRvTG93ZXJDYXNlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiByZXR1cm4gdGhlIGluc3RhbmNlIG9mIHRoZSBnaXZlbiBjb250cm9sbGVyXHJcbiAgICAgKiBAcGFyYW0gY29udHJvbGxlciBuYW1lIG9yIHR5cGUgb2YgY29udHJvbGxlciB0aGF0IHlvdSB3b3VsZCBnZXRcclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldENvbnRyb2xsZXI8VD4oY29udHJvbGxlcik6IFQge1xyXG4gICAgICAgIGlmIChjb250cm9sbGVyKVxyXG4gICAgICAgICAgICByZXR1cm4gQ3RybHNbY29udHJvbGxlcl0gfHwgQ3RybHNbY29udHJvbGxlci5uYW1lXTtcclxuXHJcbiAgICAgICAgcmV0dXJuIDxhbnk+bnVsbDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIHlvdSBjb3VsZCBjYWxsIHRoaXMgdG8gc3RhcnQgdGhlIHNlcnZlciBvbiBhIHBvcnQuIFlvdSBjb3VsZCBhbHNvIGNhbGwgbWV0aG9kIGdldEV4cHJlc3NJbnN0YW5jZSgpLmxpc3Rlbihwb3J0KTtcclxuICAgICAqIEBwYXJhbSBwb3J0IHZhbHVlIHRoYXQgcmVmZXIgdG8gdGhlIHBvcnQgb2YgdGhlIHNlcnZlclxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgbGlzdGVuKHBvcnQpOiBXZWJBcGlTZXJ2ZXIge1xyXG4gICAgICAgIGxldCBzZWxmID0gdGhpcztcclxuICAgICAgICBzZWxmLmFwcC5saXN0ZW4ocG9ydCwgKGVycjogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChlcnIpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBjb25zb2xlLmxvZygnc29tZXRoaW5nIGJhZCBoYXBwZW5lZCcsIGVycilcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhgc2VydmVyIGlzIGxpc3RlbmluZyBvbiAke3BvcnR9YClcclxuICAgICAgICB9KVxyXG4gICAgICAgIHJldHVybiB0aGlzO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogcmV0dXJuIHRoZSBleHByZXNzIGluc3RhbmNlIHRvIG1ha2Ugc29tZSBvcGVyYXRpb24gbGlrZVxyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0RXhwcmVzc0luc3RhbmNlKCk6IEFwcGxpY2F0aW9uIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hcHA7XHJcbiAgICB9XHJcbn0iXX0=