/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*! *****************************************************************************
    Copyright (c) Di Sante Simone. All rights reserved.
    
    This file is part of lib webapinode.

    webapinode is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    webapinode is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    For GNU General Public License see <http://www.gnu.org/licenses/>.
*/
/**
 * @param {?} _route
 * @param {?} target
 * @param {?} property
 * @param {?} prefix
 * @param {?} call
 * @return {?}
 */
export function parseRoute(_route, target, property, prefix, call) {
    /** @type {?} */
    var route = _route.props.route;
    if (_route.props.parsed)
        return route;
    /** @type {?} */
    var hasPrefix = prefix != null && prefix != "";
    /** @type {?} */
    var hasRoute = _route.props.route != "" && _route.props.route != null;
    /** @type {?} */
    var controllerClassName = target.constructor.name.toLowerCase().replace("controller", "");
    // console.log("route " + route, "hasroute " + hasRoute, "hasPrefix=>" + prefix, "controllerClassName=>" + controllerClassName);
    if (!hasRoute) {
        if (property == call) {
            route = hasPrefix ? prefix : controllerClassName;
        }
        else {
            route = hasPrefix ? prefix : controllerClassName;
            route += "/" + property;
        }
    }
    else {
        route = hasPrefix ? (prefix + "/" + _route.props.route) : _route.props.route;
    }
    _route.props.parsed = true;
    // console.log("parsed=>", route);
    return route;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3VwcG9ydC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3dlYmFwaW1vZGVsLyIsInNvdXJjZXMiOlsibGliL3N1cHBvcnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFxQkEsTUFBTSxVQUFVLFVBQVUsQ0FBQyxNQUFnQixFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLElBQUk7O1FBQ25FLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUs7SUFDOUIsSUFBSSxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU07UUFBRSxPQUFPLEtBQUssQ0FBQzs7UUFFbEMsU0FBUyxHQUFHLE1BQU0sSUFBSSxJQUFJLElBQUksTUFBTSxJQUFJLEVBQUU7O1FBQzFDLFFBQVEsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssSUFBSSxFQUFFLElBQUksTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksSUFBSTs7UUFDakUsbUJBQW1CLEdBQUcsTUFBTSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUM7SUFFekYsZ0lBQWdJO0lBQ2hJLElBQUksQ0FBQyxRQUFRLEVBQUU7UUFDWCxJQUFJLFFBQVEsSUFBSSxJQUFJLEVBQUU7WUFDbEIsS0FBSyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQztTQUNwRDthQUNJO1lBQ0QsS0FBSyxHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQztZQUNqRCxLQUFLLElBQUksR0FBRyxHQUFHLFFBQVEsQ0FBQztTQUMzQjtLQUNKO1NBQ0k7UUFDRCxLQUFLLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxHQUFHLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7S0FDaEY7SUFFRCxNQUFNLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUM7SUFFM0Isa0NBQWtDO0lBQ2xDLE9BQU8sS0FBSyxDQUFDO0FBQ2pCLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuLyohICoqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqXHJcbiAgICBDb3B5cmlnaHQgKGMpIERpIFNhbnRlIFNpbW9uZS4gQWxsIHJpZ2h0cyByZXNlcnZlZC5cclxuICAgIFxyXG4gICAgVGhpcyBmaWxlIGlzIHBhcnQgb2YgbGliIHdlYmFwaW5vZGUuXHJcblxyXG4gICAgd2ViYXBpbm9kZSBpcyBmcmVlIHNvZnR3YXJlOiB5b3UgY2FuIHJlZGlzdHJpYnV0ZSBpdCBhbmQvb3IgbW9kaWZ5XHJcbiAgICBpdCB1bmRlciB0aGUgdGVybXMgb2YgdGhlIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIGFzIHB1Ymxpc2hlZCBieVxyXG4gICAgdGhlIEZyZWUgU29mdHdhcmUgRm91bmRhdGlvbiwgZWl0aGVyIHZlcnNpb24gMyBvZiB0aGUgTGljZW5zZSwgb3JcclxuICAgIChhdCB5b3VyIG9wdGlvbikgYW55IGxhdGVyIHZlcnNpb24uXHJcblxyXG4gICAgd2ViYXBpbm9kZSBpcyBkaXN0cmlidXRlZCBpbiB0aGUgaG9wZSB0aGF0IGl0IHdpbGwgYmUgdXNlZnVsLFxyXG4gICAgYnV0IFdJVEhPVVQgQU5ZIFdBUlJBTlRZOyB3aXRob3V0IGV2ZW4gdGhlIGltcGxpZWQgd2FycmFudHkgb2ZcclxuICAgIE1FUkNIQU5UQUJJTElUWSBvciBGSVRORVNTIEZPUiBBIFBBUlRJQ1VMQVIgUFVSUE9TRS4gIFNlZSB0aGVcclxuICAgIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIGZvciBtb3JlIGRldGFpbHMuXHJcblxyXG4gICAgRm9yIEdOVSBHZW5lcmFsIFB1YmxpYyBMaWNlbnNlIHNlZSA8aHR0cDovL3d3dy5nbnUub3JnL2xpY2Vuc2VzLz4uXHJcbiovXHJcblxyXG5pbXBvcnQgeyBmbk9iamVjdCB9IGZyb20gXCIuL21vZGVsc1wiO1xyXG5cclxuZXhwb3J0IGZ1bmN0aW9uIHBhcnNlUm91dGUoX3JvdXRlOiBmbk9iamVjdCwgdGFyZ2V0LCBwcm9wZXJ0eSwgcHJlZml4LCBjYWxsKSB7XHJcbiAgICBsZXQgcm91dGUgPSBfcm91dGUucHJvcHMucm91dGVcclxuICAgIGlmIChfcm91dGUucHJvcHMucGFyc2VkKSByZXR1cm4gcm91dGU7XHJcblxyXG4gICAgbGV0IGhhc1ByZWZpeCA9IHByZWZpeCAhPSBudWxsICYmIHByZWZpeCAhPSBcIlwiO1xyXG4gICAgbGV0IGhhc1JvdXRlID0gX3JvdXRlLnByb3BzLnJvdXRlICE9IFwiXCIgJiYgX3JvdXRlLnByb3BzLnJvdXRlICE9IG51bGw7XHJcbiAgICBsZXQgY29udHJvbGxlckNsYXNzTmFtZSA9IHRhcmdldC5jb25zdHJ1Y3Rvci5uYW1lLnRvTG93ZXJDYXNlKCkucmVwbGFjZShcImNvbnRyb2xsZXJcIiwgXCJcIik7XHJcblxyXG4gICAgLy8gY29uc29sZS5sb2coXCJyb3V0ZSBcIiArIHJvdXRlLCBcImhhc3JvdXRlIFwiICsgaGFzUm91dGUsIFwiaGFzUHJlZml4PT5cIiArIHByZWZpeCwgXCJjb250cm9sbGVyQ2xhc3NOYW1lPT5cIiArIGNvbnRyb2xsZXJDbGFzc05hbWUpO1xyXG4gICAgaWYgKCFoYXNSb3V0ZSkge1xyXG4gICAgICAgIGlmIChwcm9wZXJ0eSA9PSBjYWxsKSB7XHJcbiAgICAgICAgICAgIHJvdXRlID0gaGFzUHJlZml4ID8gcHJlZml4IDogY29udHJvbGxlckNsYXNzTmFtZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIHJvdXRlID0gaGFzUHJlZml4ID8gcHJlZml4IDogY29udHJvbGxlckNsYXNzTmFtZTtcclxuICAgICAgICAgICAgcm91dGUgKz0gXCIvXCIgKyBwcm9wZXJ0eTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICBlbHNlIHtcclxuICAgICAgICByb3V0ZSA9IGhhc1ByZWZpeCA/IChwcmVmaXggKyBcIi9cIiArIF9yb3V0ZS5wcm9wcy5yb3V0ZSkgOiBfcm91dGUucHJvcHMucm91dGU7XHJcbiAgICB9XHJcblxyXG4gICAgX3JvdXRlLnByb3BzLnBhcnNlZCA9IHRydWU7XHJcblxyXG4gICAgLy8gY29uc29sZS5sb2coXCJwYXJzZWQ9PlwiLCByb3V0ZSk7XHJcbiAgICByZXR1cm4gcm91dGU7XHJcbn0iXX0=