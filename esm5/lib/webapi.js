/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*! *****************************************************************************
    Copyright (c) Di Sante Simone. All rights reserved.
    
    This file is part of lib webapinode.

    webapinode is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    webapinode is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    For GNU General Public License see <http://www.gnu.org/licenses/>.
*/
import { controllers as Ctrls } from './decorators';
import { callType } from './models';
import { parseRoute } from './support';
/** @type {?} */
var express = require('express');
var WebApiServer = /** @class */ (function () {
    function WebApiServer() {
        this.app = express();
        this.mapped = {};
        this.routes = {};
    }
    /**
     * return all routes registered for this webapiserver
     */
    /**
     * return all routes registered for this webapiserver
     * @return {?}
     */
    WebApiServer.prototype.getRoutes = /**
     * return all routes registered for this webapiserver
     * @return {?}
     */
    function () {
        /** @type {?} */
        var res = [];
        for (var route in this.routes) {
            res.push(route);
        }
        return res;
    };
    /**
     * this method will start all http request setted in your controller.
     * Controller should be an object class
     *
     * @param controllers array of objects used as controller; if not null only the given objects will be used as controllers,
     * else will be used all classes decorated as @Controller
     */
    /**
     * this method will start all http request setted in your controller.
     * Controller should be an object class
     *
     * @param {?=} controllers array of objects used as controller; if not null only the given objects will be used as controllers,
     * else will be used all classes decorated as \@Controller
     * @return {?}
     */
    WebApiServer.prototype.start = /**
     * this method will start all http request setted in your controller.
     * Controller should be an object class
     *
     * @param {?=} controllers array of objects used as controller; if not null only the given objects will be used as controllers,
     * else will be used all classes decorated as \@Controller
     * @return {?}
     */
    function (controllers) {
        var e_1, _a, e_2, _b;
        /** @type {?} */
        var self = this;
        /** @type {?} */
        var _controllers = [];
        if (!controllers) {
            for (var ctrl in Ctrls) {
                _controllers.push(Ctrls[ctrl]);
            }
        }
        else {
            _controllers = controllers;
        }
        /**
         * @param {?} fn
         * @param {?} _callType
         * @param {?} ctrl
         * @return {?}
         */
        function setRoute(fn, _callType, ctrl) {
            var e_3, _a, _b;
            if (!fn)
                return;
            try {
                for (var fn_1 = tslib_1.__values(fn), fn_1_1 = fn_1.next(); !fn_1_1.done; fn_1_1 = fn_1.next()) {
                    var _fn = fn_1_1.value;
                    /** @type {?} */
                    var prefix = (Ctrls[ctrl.constructor.name].__routeprefix);
                    /** @type {?} */
                    var call = self.getCallName(_callType);
                    _fn.props.route = parseRoute(_fn, ctrl, _fn.props.propertyName, prefix, call);
                    self.routes[call + "//" + _fn.props.route] = true;
                    (_b = self.app)[call].apply(_b, tslib_1.__spread(["/" + _fn.props.route], (_fn.middlewares || []).map(function (f) { return f.bind(ctrl); }).concat([_fn.fn.bind(ctrl)])));
                }
            }
            catch (e_3_1) { e_3 = { error: e_3_1 }; }
            finally {
                try {
                    if (fn_1_1 && !fn_1_1.done && (_a = fn_1.return)) _a.call(fn_1);
                }
                finally { if (e_3) throw e_3.error; }
            }
        }
        try {
            for (var _controllers_1 = tslib_1.__values(_controllers), _controllers_1_1 = _controllers_1.next(); !_controllers_1_1.done; _controllers_1_1 = _controllers_1.next()) {
                var controller = _controllers_1_1.value;
                /** @type {?} */
                var isConstructor = controller.name != null;
                if (isConstructor) {
                    controller = new controller();
                }
                if (this.mapped[controller.constructor.name]) {
                    continue;
                }
                this.mapped[controller.constructor.name] = true;
                /** @type {?} */
                var arrFn = [
                    "__fnGet",
                    "__fnPost",
                    "__fnPut",
                    "__fnDelete",
                    "__fnPatch",
                    "__fnHead",
                    "__fnCopy",
                    "__fnLink",
                    "__fnUnlink",
                    "__fnPurge",
                    "__fnPropfind",
                    "__fnLock",
                    "__fnUnlock"
                ];
                try {
                    for (var arrFn_1 = tslib_1.__values(arrFn), arrFn_1_1 = arrFn_1.next(); !arrFn_1_1.done; arrFn_1_1 = arrFn_1.next()) {
                        var _fn = arrFn_1_1.value;
                        /** @type {?} */
                        var ctrl = Ctrls[controller.constructor.name] || controller;
                        /** @type {?} */
                        var fn = ctrl[_fn];
                        setRoute(fn, this.getCallType(_fn), ctrl);
                    }
                }
                catch (e_2_1) { e_2 = { error: e_2_1 }; }
                finally {
                    try {
                        if (arrFn_1_1 && !arrFn_1_1.done && (_b = arrFn_1.return)) _b.call(arrFn_1);
                    }
                    finally { if (e_2) throw e_2.error; }
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_controllers_1_1 && !_controllers_1_1.done && (_a = _controllers_1.return)) _a.call(_controllers_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return this;
    };
    /**
     * @private
     * @param {?} typename
     * @return {?}
     */
    WebApiServer.prototype.getCallType = /**
     * @private
     * @param {?} typename
     * @return {?}
     */
    function (typename) {
        return callType[typename.replace("__fn", "").toUpperCase()];
    };
    /**
     * @private
     * @param {?} type
     * @return {?}
     */
    WebApiServer.prototype.getCallName = /**
     * @private
     * @param {?} type
     * @return {?}
     */
    function (type) {
        return callType[type].toLowerCase();
    };
    /**
     * return the instance of the given controller
     * @param controller name or type of controller that you would get
     */
    /**
     * return the instance of the given controller
     * @template T
     * @param {?} controller name or type of controller that you would get
     * @return {?}
     */
    WebApiServer.prototype.getController = /**
     * return the instance of the given controller
     * @template T
     * @param {?} controller name or type of controller that you would get
     * @return {?}
     */
    function (controller) {
        if (controller)
            return Ctrls[controller] || Ctrls[controller.name];
        return (/** @type {?} */ (null));
    };
    /**
     * you could call this to start the server on a port. You could also call method getExpressInstance().listen(port);
     * @param port value that refer to the port of the server
     */
    /**
     * you could call this to start the server on a port. You could also call method getExpressInstance().listen(port);
     * @param {?} port value that refer to the port of the server
     * @return {?}
     */
    WebApiServer.prototype.listen = /**
     * you could call this to start the server on a port. You could also call method getExpressInstance().listen(port);
     * @param {?} port value that refer to the port of the server
     * @return {?}
     */
    function (port) {
        /** @type {?} */
        var self = this;
        self.app.listen(port, function (err) {
            if (err) {
                return console.log('something bad happened', err);
            }
            console.log("server is listening on " + port);
        });
        return this;
    };
    /**
     * return the express instance to make some operation like
     */
    /**
     * return the express instance to make some operation like
     * @return {?}
     */
    WebApiServer.prototype.getExpressInstance = /**
     * return the express instance to make some operation like
     * @return {?}
     */
    function () {
        return this.app;
    };
    return WebApiServer;
}());
export { WebApiServer };
if (false) {
    /**
     * @type {?}
     * @private
     */
    WebApiServer.prototype.app;
    /**
     * @type {?}
     * @private
     */
    WebApiServer.prototype.mapped;
    /** @type {?} */
    WebApiServer.prototype.routes;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2ViYXBpLmpzIiwic291cmNlUm9vdCI6Im5nOi8vd2ViYXBpbW9kZWwvIiwic291cmNlcyI6WyJsaWIvd2ViYXBpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFdBQVcsSUFBSSxLQUFLLEVBQUUsTUFBTSxjQUFjLENBQUE7QUFDbkQsT0FBTyxFQUFZLFFBQVEsRUFBRSxNQUFNLFVBQVUsQ0FBQztBQUM5QyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sV0FBVyxDQUFDOztJQUluQyxPQUFPLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQztBQUdoQztJQVFJO1FBTlEsUUFBRyxHQUFnQixPQUFPLEVBQUUsQ0FBQztRQUU3QixXQUFNLEdBQUcsRUFBRSxDQUFDO1FBRXBCLFdBQU0sR0FBRyxFQUFFLENBQUM7SUFFSSxDQUFDO0lBRWpCOztPQUVHOzs7OztJQUNILGdDQUFTOzs7O0lBQVQ7O1lBQ1EsR0FBRyxHQUFRLEVBQUU7UUFDakIsS0FBSyxJQUFJLEtBQUssSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQzNCLEdBQUcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDbkI7UUFDRCxPQUFPLEdBQUcsQ0FBQztJQUNmLENBQUM7SUFFRDs7Ozs7O09BTUc7Ozs7Ozs7OztJQUNJLDRCQUFLOzs7Ozs7OztJQUFaLFVBQWEsV0FBd0I7OztZQUM3QixJQUFJLEdBQUcsSUFBSTs7WUFFWCxZQUFZLEdBQWUsRUFBRTtRQUNqQyxJQUFJLENBQUMsV0FBVyxFQUFFO1lBQ2QsS0FBSyxJQUFJLElBQUksSUFBSSxLQUFLLEVBQUU7Z0JBQ3BCLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7YUFDbEM7U0FDSjthQUNJO1lBQ0QsWUFBWSxHQUFHLFdBQVcsQ0FBQztTQUM5Qjs7Ozs7OztRQUNELFNBQVMsUUFBUSxDQUFDLEVBQWMsRUFBRSxTQUFtQixFQUFFLElBQUk7O1lBQ3ZELElBQUksQ0FBQyxFQUFFO2dCQUFFLE9BQU87O2dCQUNoQixLQUFnQixJQUFBLE9BQUEsaUJBQUEsRUFBRSxDQUFBLHNCQUFBLHNDQUFFO29CQUFmLElBQUksR0FBRyxlQUFBOzt3QkFDSixNQUFNLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxhQUFhLENBQUM7O3dCQUNyRCxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUM7b0JBQ3RDLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLEdBQUcsQ0FBQyxLQUFLLENBQUMsWUFBWSxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztvQkFDOUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEdBQUcsSUFBSSxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsSUFBSSxDQUFDO29CQUNsRCxDQUFBLEtBQUEsSUFBSSxDQUFDLEdBQUcsQ0FBQSxDQUFDLElBQUksQ0FBQyw2QkFBQyxHQUFHLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUssQ0FBQyxHQUFHLENBQUMsV0FBVyxJQUFJLEVBQUUsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFBLENBQUMsSUFBSSxPQUFBLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQVosQ0FBWSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFFO2lCQUN4SDs7Ozs7Ozs7O1FBQ0wsQ0FBQzs7WUFFRCxLQUF1QixJQUFBLGlCQUFBLGlCQUFBLFlBQVksQ0FBQSwwQ0FBQSxvRUFBRTtnQkFBaEMsSUFBSSxVQUFVLHlCQUFBOztvQkFFWCxhQUFhLEdBQUcsVUFBVSxDQUFDLElBQUksSUFBSSxJQUFJO2dCQUMzQyxJQUFJLGFBQWEsRUFBRTtvQkFDZixVQUFVLEdBQUcsSUFBSSxVQUFVLEVBQUUsQ0FBQztpQkFDakM7Z0JBRUQsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQzFDLFNBQVM7aUJBQ1o7Z0JBRUQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQzs7b0JBRTVDLEtBQUssR0FBRztvQkFDUixTQUFTO29CQUNULFVBQVU7b0JBQ1YsU0FBUztvQkFDVCxZQUFZO29CQUNaLFdBQVc7b0JBQ1gsVUFBVTtvQkFDVixVQUFVO29CQUNWLFVBQVU7b0JBQ1YsWUFBWTtvQkFDWixXQUFXO29CQUNYLGNBQWM7b0JBQ2QsVUFBVTtvQkFDVixZQUFZO2lCQUNmOztvQkFDRCxLQUFnQixJQUFBLFVBQUEsaUJBQUEsS0FBSyxDQUFBLDRCQUFBLCtDQUFFO3dCQUFsQixJQUFJLEdBQUcsa0JBQUE7OzRCQUNKLElBQUksR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxVQUFVOzs0QkFDdkQsRUFBRSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUM7d0JBRWxCLFFBQVEsQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBRSxJQUFJLENBQUMsQ0FBQztxQkFDN0M7Ozs7Ozs7OzthQUNKOzs7Ozs7Ozs7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDOzs7Ozs7SUFFTyxrQ0FBVzs7Ozs7SUFBbkIsVUFBb0IsUUFBZ0I7UUFDaEMsT0FBTyxRQUFRLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQztJQUNoRSxDQUFDOzs7Ozs7SUFFTyxrQ0FBVzs7Ozs7SUFBbkIsVUFBb0IsSUFBYztRQUM5QixPQUFPLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUN4QyxDQUFDO0lBRUQ7OztPQUdHOzs7Ozs7O0lBQ0ksb0NBQWE7Ozs7OztJQUFwQixVQUF3QixVQUFVO1FBQzlCLElBQUksVUFBVTtZQUNWLE9BQU8sS0FBSyxDQUFDLFVBQVUsQ0FBQyxJQUFJLEtBQUssQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7UUFFdkQsT0FBTyxtQkFBSyxJQUFJLEVBQUEsQ0FBQztJQUNyQixDQUFDO0lBRUQ7OztPQUdHOzs7Ozs7SUFDSSw2QkFBTTs7Ozs7SUFBYixVQUFjLElBQUk7O1lBQ1YsSUFBSSxHQUFHLElBQUk7UUFDZixJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsVUFBQyxHQUFXO1lBQzlCLElBQUksR0FBRyxFQUFFO2dCQUNMLE9BQU8sT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsRUFBRSxHQUFHLENBQUMsQ0FBQTthQUNwRDtZQUNELE9BQU8sQ0FBQyxHQUFHLENBQUMsNEJBQTBCLElBQU0sQ0FBQyxDQUFBO1FBQ2pELENBQUMsQ0FBQyxDQUFBO1FBQ0YsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVEOztPQUVHOzs7OztJQUNJLHlDQUFrQjs7OztJQUF6QjtRQUNJLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQztJQUNwQixDQUFDO0lBQ0wsbUJBQUM7QUFBRCxDQUFDLEFBaklELElBaUlDOzs7Ozs7O0lBL0hHLDJCQUFxQzs7Ozs7SUFFckMsOEJBQW9COztJQUVwQiw4QkFBWSIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4vKiEgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcclxuICAgIENvcHlyaWdodCAoYykgRGkgU2FudGUgU2ltb25lLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxyXG4gICAgXHJcbiAgICBUaGlzIGZpbGUgaXMgcGFydCBvZiBsaWIgd2ViYXBpbm9kZS5cclxuXHJcbiAgICB3ZWJhcGlub2RlIGlzIGZyZWUgc29mdHdhcmU6IHlvdSBjYW4gcmVkaXN0cmlidXRlIGl0IGFuZC9vciBtb2RpZnlcclxuICAgIGl0IHVuZGVyIHRoZSB0ZXJtcyBvZiB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgYXMgcHVibGlzaGVkIGJ5XHJcbiAgICB0aGUgRnJlZSBTb2Z0d2FyZSBGb3VuZGF0aW9uLCBlaXRoZXIgdmVyc2lvbiAzIG9mIHRoZSBMaWNlbnNlLCBvclxyXG4gICAgKGF0IHlvdXIgb3B0aW9uKSBhbnkgbGF0ZXIgdmVyc2lvbi5cclxuXHJcbiAgICB3ZWJhcGlub2RlIGlzIGRpc3RyaWJ1dGVkIGluIHRoZSBob3BlIHRoYXQgaXQgd2lsbCBiZSB1c2VmdWwsXHJcbiAgICBidXQgV0lUSE9VVCBBTlkgV0FSUkFOVFk7IHdpdGhvdXQgZXZlbiB0aGUgaW1wbGllZCB3YXJyYW50eSBvZlxyXG4gICAgTUVSQ0hBTlRBQklMSVRZIG9yIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFLiAgU2VlIHRoZVxyXG4gICAgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgZm9yIG1vcmUgZGV0YWlscy5cclxuXHJcbiAgICBGb3IgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2Ugc2VlIDxodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvPi5cclxuKi9cclxuXHJcbmltcG9ydCB7IGNvbnRyb2xsZXJzIGFzIEN0cmxzIH0gZnJvbSAnLi9kZWNvcmF0b3JzJ1xyXG5pbXBvcnQgeyBmbk9iamVjdCwgY2FsbFR5cGUgfSBmcm9tICcuL21vZGVscyc7XHJcbmltcG9ydCB7IHBhcnNlUm91dGUgfSBmcm9tICcuL3N1cHBvcnQnO1xyXG5pbXBvcnQgeyBBcHBsaWNhdGlvbiB9IGZyb20gJ2V4cHJlc3MnXHJcbmRlY2xhcmUgdmFyIHJlcXVpcmU6IGFueVxyXG5cclxudmFyIGV4cHJlc3MgPSByZXF1aXJlKCdleHByZXNzJyk7XHJcblxyXG5cclxuZXhwb3J0IGNsYXNzIFdlYkFwaVNlcnZlciB7XHJcblxyXG4gICAgcHJpdmF0ZSBhcHA6IEFwcGxpY2F0aW9uID0gZXhwcmVzcygpO1xyXG5cclxuICAgIHByaXZhdGUgbWFwcGVkID0ge307XHJcblxyXG4gICAgcm91dGVzID0ge307XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7IH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIHJldHVybiBhbGwgcm91dGVzIHJlZ2lzdGVyZWQgZm9yIHRoaXMgd2ViYXBpc2VydmVyXHJcbiAgICAgKi9cclxuICAgIGdldFJvdXRlcygpIHtcclxuICAgICAgICBsZXQgcmVzOiBhbnkgPSBbXTtcclxuICAgICAgICBmb3IgKHZhciByb3V0ZSBpbiB0aGlzLnJvdXRlcykge1xyXG4gICAgICAgICAgICByZXMucHVzaChyb3V0ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiByZXM7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiB0aGlzIG1ldGhvZCB3aWxsIHN0YXJ0IGFsbCBodHRwIHJlcXVlc3Qgc2V0dGVkIGluIHlvdXIgY29udHJvbGxlci4gXHJcbiAgICAgKiBDb250cm9sbGVyIHNob3VsZCBiZSBhbiBvYmplY3QgY2xhc3NcclxuICAgICAqIFxyXG4gICAgICogQHBhcmFtIGNvbnRyb2xsZXJzIGFycmF5IG9mIG9iamVjdHMgdXNlZCBhcyBjb250cm9sbGVyOyBpZiBub3QgbnVsbCBvbmx5IHRoZSBnaXZlbiBvYmplY3RzIHdpbGwgYmUgdXNlZCBhcyBjb250cm9sbGVycyxcclxuICAgICAqIGVsc2Ugd2lsbCBiZSB1c2VkIGFsbCBjbGFzc2VzIGRlY29yYXRlZCBhcyBAQ29udHJvbGxlciBcclxuICAgICAqL1xyXG4gICAgcHVibGljIHN0YXJ0KGNvbnRyb2xsZXJzPzogQXJyYXk8YW55Pik6IFdlYkFwaVNlcnZlciB7XHJcbiAgICAgICAgbGV0IHNlbGYgPSB0aGlzO1xyXG5cclxuICAgICAgICBsZXQgX2NvbnRyb2xsZXJzOiBBcnJheTxhbnk+ID0gW107XHJcbiAgICAgICAgaWYgKCFjb250cm9sbGVycykge1xyXG4gICAgICAgICAgICBmb3IgKGxldCBjdHJsIGluIEN0cmxzKSB7XHJcbiAgICAgICAgICAgICAgICBfY29udHJvbGxlcnMucHVzaChDdHJsc1tjdHJsXSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIF9jb250cm9sbGVycyA9IGNvbnRyb2xsZXJzO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmdW5jdGlvbiBzZXRSb3V0ZShmbjogZm5PYmplY3RbXSwgX2NhbGxUeXBlOiBjYWxsVHlwZSwgY3RybCkge1xyXG4gICAgICAgICAgICBpZiAoIWZuKSByZXR1cm47XHJcbiAgICAgICAgICAgIGZvciAobGV0IF9mbiBvZiBmbikge1xyXG4gICAgICAgICAgICAgICAgbGV0IHByZWZpeCA9IChDdHJsc1tjdHJsLmNvbnN0cnVjdG9yLm5hbWVdLl9fcm91dGVwcmVmaXgpO1xyXG4gICAgICAgICAgICAgICAgbGV0IGNhbGwgPSBzZWxmLmdldENhbGxOYW1lKF9jYWxsVHlwZSk7XHJcbiAgICAgICAgICAgICAgICBfZm4ucHJvcHMucm91dGUgPSBwYXJzZVJvdXRlKF9mbiwgY3RybCwgX2ZuLnByb3BzLnByb3BlcnR5TmFtZSwgcHJlZml4LCBjYWxsKTtcclxuICAgICAgICAgICAgICAgIHNlbGYucm91dGVzW2NhbGwgKyBcIi8vXCIgKyBfZm4ucHJvcHMucm91dGVdID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIHNlbGYuYXBwW2NhbGxdKFwiL1wiICsgX2ZuLnByb3BzLnJvdXRlLCAuLi4oX2ZuLm1pZGRsZXdhcmVzIHx8IFtdKS5tYXAoZiA9PiBmLmJpbmQoY3RybCkpLmNvbmNhdChbX2ZuLmZuLmJpbmQoY3RybCldKSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGZvciAobGV0IGNvbnRyb2xsZXIgb2YgX2NvbnRyb2xsZXJzKSB7XHJcblxyXG4gICAgICAgICAgICB2YXIgaXNDb25zdHJ1Y3RvciA9IGNvbnRyb2xsZXIubmFtZSAhPSBudWxsO1xyXG4gICAgICAgICAgICBpZiAoaXNDb25zdHJ1Y3Rvcikge1xyXG4gICAgICAgICAgICAgICAgY29udHJvbGxlciA9IG5ldyBjb250cm9sbGVyKCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLm1hcHBlZFtjb250cm9sbGVyLmNvbnN0cnVjdG9yLm5hbWVdKSB7XHJcbiAgICAgICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5tYXBwZWRbY29udHJvbGxlci5jb25zdHJ1Y3Rvci5uYW1lXSA9IHRydWU7XHJcblxyXG4gICAgICAgICAgICBsZXQgYXJyRm4gPSBbXHJcbiAgICAgICAgICAgICAgICBcIl9fZm5HZXRcIixcclxuICAgICAgICAgICAgICAgIFwiX19mblBvc3RcIixcclxuICAgICAgICAgICAgICAgIFwiX19mblB1dFwiLFxyXG4gICAgICAgICAgICAgICAgXCJfX2ZuRGVsZXRlXCIsXHJcbiAgICAgICAgICAgICAgICBcIl9fZm5QYXRjaFwiLFxyXG4gICAgICAgICAgICAgICAgXCJfX2ZuSGVhZFwiLFxyXG4gICAgICAgICAgICAgICAgXCJfX2ZuQ29weVwiLFxyXG4gICAgICAgICAgICAgICAgXCJfX2ZuTGlua1wiLFxyXG4gICAgICAgICAgICAgICAgXCJfX2ZuVW5saW5rXCIsXHJcbiAgICAgICAgICAgICAgICBcIl9fZm5QdXJnZVwiLFxyXG4gICAgICAgICAgICAgICAgXCJfX2ZuUHJvcGZpbmRcIixcclxuICAgICAgICAgICAgICAgIFwiX19mbkxvY2tcIixcclxuICAgICAgICAgICAgICAgIFwiX19mblVubG9ja1wiXHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICAgICAgZm9yIChsZXQgX2ZuIG9mIGFyckZuKSB7XHJcbiAgICAgICAgICAgICAgICBsZXQgY3RybCA9IEN0cmxzW2NvbnRyb2xsZXIuY29uc3RydWN0b3IubmFtZV0gfHwgY29udHJvbGxlcjtcclxuICAgICAgICAgICAgICAgIGxldCBmbiA9IGN0cmxbX2ZuXTtcclxuXHJcbiAgICAgICAgICAgICAgICBzZXRSb3V0ZShmbiwgdGhpcy5nZXRDYWxsVHlwZShfZm4pLCBjdHJsKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcztcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldENhbGxUeXBlKHR5cGVuYW1lOiBzdHJpbmcpOiBjYWxsVHlwZSB7XHJcbiAgICAgICAgcmV0dXJuIGNhbGxUeXBlW3R5cGVuYW1lLnJlcGxhY2UoXCJfX2ZuXCIsIFwiXCIpLnRvVXBwZXJDYXNlKCldO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0Q2FsbE5hbWUodHlwZTogY2FsbFR5cGUpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBjYWxsVHlwZVt0eXBlXS50b0xvd2VyQ2FzZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogcmV0dXJuIHRoZSBpbnN0YW5jZSBvZiB0aGUgZ2l2ZW4gY29udHJvbGxlclxyXG4gICAgICogQHBhcmFtIGNvbnRyb2xsZXIgbmFtZSBvciB0eXBlIG9mIGNvbnRyb2xsZXIgdGhhdCB5b3Ugd291bGQgZ2V0XHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRDb250cm9sbGVyPFQ+KGNvbnRyb2xsZXIpOiBUIHtcclxuICAgICAgICBpZiAoY29udHJvbGxlcilcclxuICAgICAgICAgICAgcmV0dXJuIEN0cmxzW2NvbnRyb2xsZXJdIHx8IEN0cmxzW2NvbnRyb2xsZXIubmFtZV07XHJcblxyXG4gICAgICAgIHJldHVybiA8YW55Pm51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiB5b3UgY291bGQgY2FsbCB0aGlzIHRvIHN0YXJ0IHRoZSBzZXJ2ZXIgb24gYSBwb3J0LiBZb3UgY291bGQgYWxzbyBjYWxsIG1ldGhvZCBnZXRFeHByZXNzSW5zdGFuY2UoKS5saXN0ZW4ocG9ydCk7XHJcbiAgICAgKiBAcGFyYW0gcG9ydCB2YWx1ZSB0aGF0IHJlZmVyIHRvIHRoZSBwb3J0IG9mIHRoZSBzZXJ2ZXJcclxuICAgICAqL1xyXG4gICAgcHVibGljIGxpc3Rlbihwb3J0KTogV2ViQXBpU2VydmVyIHtcclxuICAgICAgICBsZXQgc2VsZiA9IHRoaXM7XHJcbiAgICAgICAgc2VsZi5hcHAubGlzdGVuKHBvcnQsIChlcnI6IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZXJyKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gY29uc29sZS5sb2coJ3NvbWV0aGluZyBiYWQgaGFwcGVuZWQnLCBlcnIpXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY29uc29sZS5sb2coYHNlcnZlciBpcyBsaXN0ZW5pbmcgb24gJHtwb3J0fWApXHJcbiAgICAgICAgfSlcclxuICAgICAgICByZXR1cm4gdGhpcztcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIHJldHVybiB0aGUgZXhwcmVzcyBpbnN0YW5jZSB0byBtYWtlIHNvbWUgb3BlcmF0aW9uIGxpa2VcclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldEV4cHJlc3NJbnN0YW5jZSgpOiBBcHBsaWNhdGlvbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYXBwO1xyXG4gICAgfVxyXG59Il19