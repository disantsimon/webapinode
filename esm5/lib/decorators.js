/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*! *****************************************************************************
    Copyright (c) Di Sante Simone. All rights reserved.
    
    This file is part of lib webapinode.

    webapinode is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    webapinode is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    For GNU General Public License see <http://www.gnu.org/licenses/>.
*/
/** @type {?} */
export var controllers = {};
/**
 * This decorator set a class as controller, so each function decorated with a Http decorator function will be a route for your api
 *
 * 1) As you can see Controller accepts one argument (ex "user"), if given that will be the name of your controller. If you define a function get()
 * that could be reached, if decorated with HttpGET(), with a simple GET request as user. If argument name is not given, then the http will start
 * with class name, example class UserController{} then name will simply user
 *
 * \@Controller() //1)
 * class MyController{
 *
 * }
 *
 * another example
 *
 * in this case each http request start with just us/, because "us" is the given name by decorator
 * \@Controller("us")
 * class User{}
 *
 * @param {?=} name give a name to the class(controller)
 * @return {?}
 */
export function Controller(name) {
    return function Controller(target) {
        name = name || "";
        /** @type {?} */
        var opt = {
            value: name.trim ? name.trim() : name,
            enumerable: false,
            writable: false,
            configurable: false
        };
        controllers[target.name] = new target;
        Object.defineProperty(controllers[target.name], "__routeprefix", opt);
    };
}
/**
 * @param {?} fns
 * @param {?} target
 * @param {?} propertyName
 * @param {?} middlewares
 * @param {?} route
 * @return {?}
 */
function addFunction(fns, target, propertyName, middlewares, route) {
    fns.push({
        fn: target[propertyName],
        middlewares: middlewares,
        props: { route: route, propertyName: propertyName }
    });
}
/**
 * @param {?} target
 * @param {?} fn
 * @return {?}
 */
function init(target, fn) {
    if (Object.getOwnPropertyDescriptor(target, fn) == undefined) {
        Object.defineProperty(target, fn, {
            value: [],
            enumerable: false,
            writable: false
        });
    }
}
// * 
//  * @Controller
//  * class UserController{
//  * 
//  *  @HttpGET()
//  *  get(request,response,next){
//  *      //request GET to user  
//  *  }  
//  * 
//  *  @HttpGET()
//  *  getByName(request,response,next){
//  *      //request GET to user/getbyname  
//  *  }  
//  * 
//  *  @HttpGET("status")
//  *  getStatus(request,response,next){
//  *      //request GET to user/status  
//  *  }  
//  * 
//  * }
//  *  
/**
 * that set a Controller function as a simple Http GET request
 * @param {?=} route name of the Http GET request, if null will be automatically replaced by function name if that name is different to get
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
export function HttpGET(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnGet");
        addFunction(target.__fnGet, target, propertyName, middlewares, route);
    };
}
/**
 * that set a Controller function as a simple Http POST request
 * @param {?=} route name of the Http POST request, if null will be automatically replaced by function name if that name is different to post
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
export function HttpPOST(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnPost");
        addFunction(target.__fnPost, target, propertyName, middlewares, route);
    };
}
/**
 * that set a Controller function as a simple Http PUT request
 * @param {?=} route name of the Http PUT request, if null will be automatically replaced by function name if that name is different to put
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
export function HttpPUT(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnPut");
        addFunction(target.__fnPut, target, propertyName, middlewares, route);
    };
}
/**
 * that set a Controller function as a simple Http DELETE request
 * @param {?=} route name of the Http DELETE request, if null will be automatically replaced by function name if that name is different to delete
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
export function HttpDELETE(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnDelete");
        addFunction(target.__fnDelete, target, propertyName, middlewares, route);
    };
}
/**
 * that set a Controller function as a simple Http PATCH request
 * @param {?=} route name of the Http PATCH request, if null will be automatically replaced by function name if that name is different to patch
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
export function HttpPATCH(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnPatch");
        addFunction(target.__fnPatch, target, propertyName, middlewares, route);
    };
}
/**
 * that set a Controller function as a simple Http HEAD request
 * @param {?=} route name of the Http HEAD request, if null will be automatically replaced by function name if that name is different to head
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
export function HttpHEAD(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnHead");
        addFunction(target.__fnHead, target, propertyName, middlewares, route);
    };
}
/**
 * that set a Controller function as a simple Http COPY request
 * @param {?=} route name of the Http COPY request, if null will be automatically replaced by function name if that name is different to copy
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
export function HttpCOPY(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnCopy");
        addFunction(target.__fnCopy, target, propertyName, middlewares, route);
    };
}
/**
 * that set a Controller function as a simple Http LINK request
 * @param {?=} route name of the Http LINK request, if null will be automatically replaced by function name if that name is different to link
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
export function HttpLINK(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnLink");
        addFunction(target.__fnLink, target, propertyName, middlewares, route);
    };
}
/**
 * that set a Controller function as a simple Http UNLINK request
 * @param {?=} route name of the Http UNLINK request, if null will be automatically replaced by function name if that name is different to unlink
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
export function HttpUNLINK(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnUnlink");
        addFunction(target.__fnUnlink, target, propertyName, middlewares, route);
    };
}
/**
 * that set a Controller function as a simple Http PURGE request
 * @param {?=} route name of the Http PURGE request, if null will be automatically replaced by function name if that name is different to purge
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
export function HttpPURGE(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnPurge");
        addFunction(target.__fnPurge, target, propertyName, middlewares, route);
    };
}
/**
 * that set a Controller function as a simple Http PROPFIND request
 * @param {?=} route name of the Http PROPFIND request, if null will be automatically replaced by function name if that name is different to propfind
 * @param {?=} middlewares all middleware functions for the request
 * @return {?}
 */
export function HttpPROPFIND(route, middlewares) {
    return function (target, propertyName) {
        init(target, "__fnPropfind");
        addFunction(target.__fnPropfind, target, propertyName, middlewares, route);
    };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVjb3JhdG9ycy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL3dlYmFwaW1vZGVsLyIsInNvdXJjZXMiOlsibGliL2RlY29yYXRvcnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW9CQSxNQUFNLEtBQUssV0FBVyxHQUFHLEVBRXhCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBc0JELE1BQU0sVUFBVSxVQUFVLENBQUMsSUFBSztJQUM1QixPQUFPLFNBQVMsVUFBVSxDQUFDLE1BQU07UUFDN0IsSUFBSSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7O1lBQ2QsR0FBRyxHQUF1QjtZQUMxQixLQUFLLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJO1lBQ3JDLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLFFBQVEsRUFBRSxLQUFLO1lBQ2YsWUFBWSxFQUFFLEtBQUs7U0FDdEI7UUFFRCxXQUFXLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksTUFBTSxDQUFDO1FBQ3RDLE1BQU0sQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxlQUFlLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDMUUsQ0FBQyxDQUFBO0FBQ0wsQ0FBQzs7Ozs7Ozs7O0FBRUQsU0FBUyxXQUFXLENBQUMsR0FBRyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsV0FBVyxFQUFFLEtBQUs7SUFFOUQsR0FBRyxDQUFDLElBQUksQ0FBQztRQUNMLEVBQUUsRUFBRSxNQUFNLENBQUMsWUFBWSxDQUFDO1FBQ3hCLFdBQVcsRUFBRSxXQUFXO1FBQ3hCLEtBQUssRUFBRSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLFlBQVksRUFBRTtLQUN0RCxDQUFDLENBQUM7QUFDUCxDQUFDOzs7Ozs7QUFFRCxTQUFTLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBRTtJQUNwQixJQUFJLE1BQU0sQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLElBQUksU0FBUyxFQUFFO1FBQzFELE1BQU0sQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLEVBQUUsRUFBRTtZQUM5QixLQUFLLEVBQUUsRUFBRTtZQUNULFVBQVUsRUFBRSxLQUFLO1lBQ2pCLFFBQVEsRUFBRSxLQUFLO1NBQ2xCLENBQUMsQ0FBQTtLQUNMO0FBQ0wsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQTZCRCxNQUFNLFVBQVUsT0FBTyxDQUFDLEtBQWMsRUFBRSxXQUF5RDtJQUM3RixPQUFPLFVBQVUsTUFBTSxFQUFFLFlBQVk7UUFDakMsSUFBSSxDQUFDLE1BQU0sRUFBRSxTQUFTLENBQUMsQ0FBQztRQUN4QixXQUFXLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMxRSxDQUFDLENBQUE7QUFDTCxDQUFDOzs7Ozs7O0FBT0QsTUFBTSxVQUFVLFFBQVEsQ0FBQyxLQUFjLEVBQUUsV0FBeUQ7SUFDOUYsT0FBTyxVQUFVLE1BQU0sRUFBRSxZQUFZO1FBQ2pDLElBQUksQ0FBQyxNQUFNLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDekIsV0FBVyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDM0UsQ0FBQyxDQUFBO0FBQ0wsQ0FBQzs7Ozs7OztBQU9ELE1BQU0sVUFBVSxPQUFPLENBQUMsS0FBYyxFQUFFLFdBQXlEO0lBQzdGLE9BQU8sVUFBVSxNQUFNLEVBQUUsWUFBWTtRQUNqQyxJQUFJLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQ3hCLFdBQVcsQ0FBQyxNQUFNLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzFFLENBQUMsQ0FBQTtBQUNMLENBQUM7Ozs7Ozs7QUFPRCxNQUFNLFVBQVUsVUFBVSxDQUFDLEtBQWMsRUFBRSxXQUF5RDtJQUNoRyxPQUFPLFVBQVUsTUFBTSxFQUFFLFlBQVk7UUFDakMsSUFBSSxDQUFDLE1BQU0sRUFBRSxZQUFZLENBQUMsQ0FBQztRQUMzQixXQUFXLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUM3RSxDQUFDLENBQUE7QUFDTCxDQUFDOzs7Ozs7O0FBTUQsTUFBTSxVQUFVLFNBQVMsQ0FBQyxLQUFjLEVBQUUsV0FBeUQ7SUFDL0YsT0FBTyxVQUFVLE1BQU0sRUFBRSxZQUFZO1FBQ2pDLElBQUksQ0FBQyxNQUFNLEVBQUUsV0FBVyxDQUFDLENBQUM7UUFDMUIsV0FBVyxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDNUUsQ0FBQyxDQUFBO0FBQ0wsQ0FBQzs7Ozs7OztBQU1ELE1BQU0sVUFBVSxRQUFRLENBQUMsS0FBYyxFQUFFLFdBQXlEO0lBQzlGLE9BQU8sVUFBVSxNQUFNLEVBQUUsWUFBWTtRQUNqQyxJQUFJLENBQUMsTUFBTSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBQ3pCLFdBQVcsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzNFLENBQUMsQ0FBQTtBQUNMLENBQUM7Ozs7Ozs7QUFNRCxNQUFNLFVBQVUsUUFBUSxDQUFDLEtBQWMsRUFBRSxXQUF5RDtJQUM5RixPQUFPLFVBQVUsTUFBTSxFQUFFLFlBQVk7UUFDakMsSUFBSSxDQUFDLE1BQU0sRUFBRSxVQUFVLENBQUMsQ0FBQztRQUN6QixXQUFXLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUMzRSxDQUFDLENBQUE7QUFDTCxDQUFDOzs7Ozs7O0FBTUQsTUFBTSxVQUFVLFFBQVEsQ0FBQyxLQUFjLEVBQUUsV0FBeUQ7SUFDOUYsT0FBTyxVQUFVLE1BQU0sRUFBRSxZQUFZO1FBQ2pDLElBQUksQ0FBQyxNQUFNLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFDekIsV0FBVyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDM0UsQ0FBQyxDQUFBO0FBQ0wsQ0FBQzs7Ozs7OztBQU1ELE1BQU0sVUFBVSxVQUFVLENBQUMsS0FBYyxFQUFFLFdBQXlEO0lBQ2hHLE9BQU8sVUFBVSxNQUFNLEVBQUUsWUFBWTtRQUNqQyxJQUFJLENBQUMsTUFBTSxFQUFFLFlBQVksQ0FBQyxDQUFDO1FBQzNCLFdBQVcsQ0FBQyxNQUFNLENBQUMsVUFBVSxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsV0FBVyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzdFLENBQUMsQ0FBQTtBQUNMLENBQUM7Ozs7Ozs7QUFNRCxNQUFNLFVBQVUsU0FBUyxDQUFDLEtBQWMsRUFBRSxXQUF5RDtJQUMvRixPQUFPLFVBQVUsTUFBTSxFQUFFLFlBQVk7UUFDakMsSUFBSSxDQUFDLE1BQU0sRUFBRSxXQUFXLENBQUMsQ0FBQztRQUMxQixXQUFXLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLFdBQVcsRUFBRSxLQUFLLENBQUMsQ0FBQztJQUM1RSxDQUFDLENBQUE7QUFDTCxDQUFDOzs7Ozs7O0FBTUQsTUFBTSxVQUFVLFlBQVksQ0FBQyxLQUFjLEVBQUUsV0FBeUQ7SUFDbEcsT0FBTyxVQUFVLE1BQU0sRUFBRSxZQUFZO1FBQ2pDLElBQUksQ0FBQyxNQUFNLEVBQUUsY0FBYyxDQUFDLENBQUM7UUFDN0IsV0FBVyxDQUFDLE1BQU0sQ0FBQyxZQUFZLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxXQUFXLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDL0UsQ0FBQyxDQUFBO0FBQ0wsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4vKiEgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcclxuICAgIENvcHlyaWdodCAoYykgRGkgU2FudGUgU2ltb25lLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxyXG4gICAgXHJcbiAgICBUaGlzIGZpbGUgaXMgcGFydCBvZiBsaWIgd2ViYXBpbm9kZS5cclxuXHJcbiAgICB3ZWJhcGlub2RlIGlzIGZyZWUgc29mdHdhcmU6IHlvdSBjYW4gcmVkaXN0cmlidXRlIGl0IGFuZC9vciBtb2RpZnlcclxuICAgIGl0IHVuZGVyIHRoZSB0ZXJtcyBvZiB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgYXMgcHVibGlzaGVkIGJ5XHJcbiAgICB0aGUgRnJlZSBTb2Z0d2FyZSBGb3VuZGF0aW9uLCBlaXRoZXIgdmVyc2lvbiAzIG9mIHRoZSBMaWNlbnNlLCBvclxyXG4gICAgKGF0IHlvdXIgb3B0aW9uKSBhbnkgbGF0ZXIgdmVyc2lvbi5cclxuXHJcbiAgICB3ZWJhcGlub2RlIGlzIGRpc3RyaWJ1dGVkIGluIHRoZSBob3BlIHRoYXQgaXQgd2lsbCBiZSB1c2VmdWwsXHJcbiAgICBidXQgV0lUSE9VVCBBTlkgV0FSUkFOVFk7IHdpdGhvdXQgZXZlbiB0aGUgaW1wbGllZCB3YXJyYW50eSBvZlxyXG4gICAgTUVSQ0hBTlRBQklMSVRZIG9yIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFLiAgU2VlIHRoZVxyXG4gICAgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgZm9yIG1vcmUgZGV0YWlscy5cclxuXHJcbiAgICBGb3IgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2Ugc2VlIDxodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvPi5cclxuKi9cclxuXHJcblxyXG5leHBvcnQgdmFyIGNvbnRyb2xsZXJzID0ge1xyXG5cclxufTtcclxuXHJcbi8qKlxyXG4gKiBUaGlzIGRlY29yYXRvciBzZXQgYSBjbGFzcyBhcyBjb250cm9sbGVyLCBzbyBlYWNoIGZ1bmN0aW9uIGRlY29yYXRlZCB3aXRoIGEgSHR0cCBkZWNvcmF0b3IgZnVuY3Rpb24gd2lsbCBiZSBhIHJvdXRlIGZvciB5b3VyIGFwaVxyXG4gKiBcclxuICogMSkgQXMgeW91IGNhbiBzZWUgQ29udHJvbGxlciBhY2NlcHRzIG9uZSBhcmd1bWVudCAoZXggXCJ1c2VyXCIpLCBpZiBnaXZlbiB0aGF0IHdpbGwgYmUgdGhlIG5hbWUgb2YgeW91ciBjb250cm9sbGVyLiBJZiB5b3UgZGVmaW5lIGEgZnVuY3Rpb24gZ2V0KClcclxuICogdGhhdCBjb3VsZCBiZSByZWFjaGVkLCBpZiBkZWNvcmF0ZWQgd2l0aCBIdHRwR0VUKCksIHdpdGggYSBzaW1wbGUgR0VUIHJlcXVlc3QgYXMgdXNlci4gSWYgYXJndW1lbnQgbmFtZSBpcyBub3QgZ2l2ZW4sIHRoZW4gdGhlIGh0dHAgd2lsbCBzdGFydFxyXG4gKiB3aXRoIGNsYXNzIG5hbWUsIGV4YW1wbGUgY2xhc3MgVXNlckNvbnRyb2xsZXJ7fSB0aGVuIG5hbWUgd2lsbCBzaW1wbHkgdXNlclxyXG4gKiBcclxuICogQENvbnRyb2xsZXIoKSAvLzEpXHJcbiAqIGNsYXNzIE15Q29udHJvbGxlcntcclxuICogICAgICBcclxuICogfVxyXG4gKiBcclxuICogYW5vdGhlciBleGFtcGxlIFxyXG4gKiBcclxuICogaW4gdGhpcyBjYXNlIGVhY2ggaHR0cCByZXF1ZXN0IHN0YXJ0IHdpdGgganVzdCB1cy8sIGJlY2F1c2UgXCJ1c1wiIGlzIHRoZSBnaXZlbiBuYW1lIGJ5IGRlY29yYXRvciBcclxuICogQENvbnRyb2xsZXIoXCJ1c1wiKVxyXG4gKiBjbGFzcyBVc2Vye31cclxuICogXHJcbiAqIEBwYXJhbSBuYW1lIGdpdmUgYSBuYW1lIHRvIHRoZSBjbGFzcyhjb250cm9sbGVyKSBcclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBDb250cm9sbGVyKG5hbWU/KSB7XHJcbiAgICByZXR1cm4gZnVuY3Rpb24gQ29udHJvbGxlcih0YXJnZXQpIHtcclxuICAgICAgICBuYW1lID0gbmFtZSB8fCBcIlwiO1xyXG4gICAgICAgIHZhciBvcHQ6IFByb3BlcnR5RGVzY3JpcHRvciA9IHtcclxuICAgICAgICAgICAgdmFsdWU6IG5hbWUudHJpbSA/IG5hbWUudHJpbSgpIDogbmFtZSxcclxuICAgICAgICAgICAgZW51bWVyYWJsZTogZmFsc2UsXHJcbiAgICAgICAgICAgIHdyaXRhYmxlOiBmYWxzZSxcclxuICAgICAgICAgICAgY29uZmlndXJhYmxlOiBmYWxzZVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29udHJvbGxlcnNbdGFyZ2V0Lm5hbWVdID0gbmV3IHRhcmdldDtcclxuICAgICAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkoY29udHJvbGxlcnNbdGFyZ2V0Lm5hbWVdLCBcIl9fcm91dGVwcmVmaXhcIiwgb3B0KTtcclxuICAgIH1cclxufVxyXG5cclxuZnVuY3Rpb24gYWRkRnVuY3Rpb24oZm5zLCB0YXJnZXQsIHByb3BlcnR5TmFtZSwgbWlkZGxld2FyZXMsIHJvdXRlKSB7XHJcblxyXG4gICAgZm5zLnB1c2goe1xyXG4gICAgICAgIGZuOiB0YXJnZXRbcHJvcGVydHlOYW1lXSxcclxuICAgICAgICBtaWRkbGV3YXJlczogbWlkZGxld2FyZXMsXHJcbiAgICAgICAgcHJvcHM6IHsgcm91dGU6IHJvdXRlLCBwcm9wZXJ0eU5hbWU6IHByb3BlcnR5TmFtZSB9XHJcbiAgICB9KTtcclxufVxyXG5cclxuZnVuY3Rpb24gaW5pdCh0YXJnZXQsIGZuKSB7XHJcbiAgICBpZiAoT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcih0YXJnZXQsIGZuKSA9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBmbiwge1xyXG4gICAgICAgICAgICB2YWx1ZTogW10sXHJcbiAgICAgICAgICAgIGVudW1lcmFibGU6IGZhbHNlLFxyXG4gICAgICAgICAgICB3cml0YWJsZTogZmFsc2VcclxuICAgICAgICB9KVxyXG4gICAgfVxyXG59XHJcblxyXG4vLyAqIFxyXG4vLyAgKiBAQ29udHJvbGxlclxyXG4vLyAgKiBjbGFzcyBVc2VyQ29udHJvbGxlcntcclxuLy8gICogXHJcbi8vICAqICBASHR0cEdFVCgpXHJcbi8vICAqICBnZXQocmVxdWVzdCxyZXNwb25zZSxuZXh0KXtcclxuLy8gICogICAgICAvL3JlcXVlc3QgR0VUIHRvIHVzZXIgIFxyXG4vLyAgKiAgfSAgXHJcbi8vICAqIFxyXG4vLyAgKiAgQEh0dHBHRVQoKVxyXG4vLyAgKiAgZ2V0QnlOYW1lKHJlcXVlc3QscmVzcG9uc2UsbmV4dCl7XHJcbi8vICAqICAgICAgLy9yZXF1ZXN0IEdFVCB0byB1c2VyL2dldGJ5bmFtZSAgXHJcbi8vICAqICB9ICBcclxuLy8gICogXHJcbi8vICAqICBASHR0cEdFVChcInN0YXR1c1wiKVxyXG4vLyAgKiAgZ2V0U3RhdHVzKHJlcXVlc3QscmVzcG9uc2UsbmV4dCl7XHJcbi8vICAqICAgICAgLy9yZXF1ZXN0IEdFVCB0byB1c2VyL3N0YXR1cyAgXHJcbi8vICAqICB9ICBcclxuLy8gICogXHJcbi8vICAqIH1cclxuLy8gICogIFxyXG5cclxuLyoqXHJcbiAqIHRoYXQgc2V0IGEgQ29udHJvbGxlciBmdW5jdGlvbiBhcyBhIHNpbXBsZSBIdHRwIEdFVCByZXF1ZXN0XHJcbiAqIEBwYXJhbSByb3V0ZSBuYW1lIG9mIHRoZSBIdHRwIEdFVCByZXF1ZXN0LCBpZiBudWxsIHdpbGwgYmUgYXV0b21hdGljYWxseSByZXBsYWNlZCBieSBmdW5jdGlvbiBuYW1lIGlmIHRoYXQgbmFtZSBpcyBkaWZmZXJlbnQgdG8gZ2V0XHJcbiAqIEBwYXJhbSBtaWRkbGV3YXJlcyBhbGwgbWlkZGxld2FyZSBmdW5jdGlvbnMgZm9yIHRoZSByZXF1ZXN0XHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gSHR0cEdFVChyb3V0ZT86IHN0cmluZywgbWlkZGxld2FyZXM/OiBBcnJheTwocmVxdWVzdD8sIHJlc3BvbnNlPywgbmV4dD8pID0+IHZvaWQ+KSB7XHJcbiAgICByZXR1cm4gZnVuY3Rpb24gKHRhcmdldCwgcHJvcGVydHlOYW1lKSB7XHJcbiAgICAgICAgaW5pdCh0YXJnZXQsIFwiX19mbkdldFwiKTtcclxuICAgICAgICBhZGRGdW5jdGlvbih0YXJnZXQuX19mbkdldCwgdGFyZ2V0LCBwcm9wZXJ0eU5hbWUsIG1pZGRsZXdhcmVzLCByb3V0ZSk7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiB0aGF0IHNldCBhIENvbnRyb2xsZXIgZnVuY3Rpb24gYXMgYSBzaW1wbGUgSHR0cCBQT1NUIHJlcXVlc3RcclxuICogQHBhcmFtIHJvdXRlIG5hbWUgb2YgdGhlIEh0dHAgUE9TVCByZXF1ZXN0LCBpZiBudWxsIHdpbGwgYmUgYXV0b21hdGljYWxseSByZXBsYWNlZCBieSBmdW5jdGlvbiBuYW1lIGlmIHRoYXQgbmFtZSBpcyBkaWZmZXJlbnQgdG8gcG9zdFxyXG4gKiBAcGFyYW0gbWlkZGxld2FyZXMgYWxsIG1pZGRsZXdhcmUgZnVuY3Rpb25zIGZvciB0aGUgcmVxdWVzdFxyXG4gKi9cclxuZXhwb3J0IGZ1bmN0aW9uIEh0dHBQT1NUKHJvdXRlPzogc3RyaW5nLCBtaWRkbGV3YXJlcz86IEFycmF5PChyZXF1ZXN0PywgcmVzcG9uc2U/LCBuZXh0PykgPT4gdm9pZD4pIHtcclxuICAgIHJldHVybiBmdW5jdGlvbiAodGFyZ2V0LCBwcm9wZXJ0eU5hbWUpIHtcclxuICAgICAgICBpbml0KHRhcmdldCwgXCJfX2ZuUG9zdFwiKTtcclxuICAgICAgICBhZGRGdW5jdGlvbih0YXJnZXQuX19mblBvc3QsIHRhcmdldCwgcHJvcGVydHlOYW1lLCBtaWRkbGV3YXJlcywgcm91dGUpO1xyXG4gICAgfVxyXG59XHJcblxyXG4vKipcclxuICogdGhhdCBzZXQgYSBDb250cm9sbGVyIGZ1bmN0aW9uIGFzIGEgc2ltcGxlIEh0dHAgUFVUIHJlcXVlc3RcclxuICogQHBhcmFtIHJvdXRlIG5hbWUgb2YgdGhlIEh0dHAgUFVUIHJlcXVlc3QsIGlmIG51bGwgd2lsbCBiZSBhdXRvbWF0aWNhbGx5IHJlcGxhY2VkIGJ5IGZ1bmN0aW9uIG5hbWUgaWYgdGhhdCBuYW1lIGlzIGRpZmZlcmVudCB0byBwdXRcclxuICogQHBhcmFtIG1pZGRsZXdhcmVzIGFsbCBtaWRkbGV3YXJlIGZ1bmN0aW9ucyBmb3IgdGhlIHJlcXVlc3RcclxuICovXHJcbmV4cG9ydCBmdW5jdGlvbiBIdHRwUFVUKHJvdXRlPzogc3RyaW5nLCBtaWRkbGV3YXJlcz86IEFycmF5PChyZXF1ZXN0PywgcmVzcG9uc2U/LCBuZXh0PykgPT4gdm9pZD4pIHtcclxuICAgIHJldHVybiBmdW5jdGlvbiAodGFyZ2V0LCBwcm9wZXJ0eU5hbWUpIHtcclxuICAgICAgICBpbml0KHRhcmdldCwgXCJfX2ZuUHV0XCIpO1xyXG4gICAgICAgIGFkZEZ1bmN0aW9uKHRhcmdldC5fX2ZuUHV0LCB0YXJnZXQsIHByb3BlcnR5TmFtZSwgbWlkZGxld2FyZXMsIHJvdXRlKTtcclxuICAgIH1cclxufVxyXG5cclxuLyoqXHJcbiAqIHRoYXQgc2V0IGEgQ29udHJvbGxlciBmdW5jdGlvbiBhcyBhIHNpbXBsZSBIdHRwIERFTEVURSByZXF1ZXN0XHJcbiAqIEBwYXJhbSByb3V0ZSBuYW1lIG9mIHRoZSBIdHRwIERFTEVURSByZXF1ZXN0LCBpZiBudWxsIHdpbGwgYmUgYXV0b21hdGljYWxseSByZXBsYWNlZCBieSBmdW5jdGlvbiBuYW1lIGlmIHRoYXQgbmFtZSBpcyBkaWZmZXJlbnQgdG8gZGVsZXRlXHJcbiAqIEBwYXJhbSBtaWRkbGV3YXJlcyBhbGwgbWlkZGxld2FyZSBmdW5jdGlvbnMgZm9yIHRoZSByZXF1ZXN0XHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gSHR0cERFTEVURShyb3V0ZT86IHN0cmluZywgbWlkZGxld2FyZXM/OiBBcnJheTwocmVxdWVzdD8sIHJlc3BvbnNlPywgbmV4dD8pID0+IHZvaWQ+KSB7XHJcbiAgICByZXR1cm4gZnVuY3Rpb24gKHRhcmdldCwgcHJvcGVydHlOYW1lKSB7XHJcbiAgICAgICAgaW5pdCh0YXJnZXQsIFwiX19mbkRlbGV0ZVwiKTtcclxuICAgICAgICBhZGRGdW5jdGlvbih0YXJnZXQuX19mbkRlbGV0ZSwgdGFyZ2V0LCBwcm9wZXJ0eU5hbWUsIG1pZGRsZXdhcmVzLCByb3V0ZSk7XHJcbiAgICB9XHJcbn1cclxuLyoqXHJcbiAqIHRoYXQgc2V0IGEgQ29udHJvbGxlciBmdW5jdGlvbiBhcyBhIHNpbXBsZSBIdHRwIFBBVENIIHJlcXVlc3RcclxuICogQHBhcmFtIHJvdXRlIG5hbWUgb2YgdGhlIEh0dHAgUEFUQ0ggcmVxdWVzdCwgaWYgbnVsbCB3aWxsIGJlIGF1dG9tYXRpY2FsbHkgcmVwbGFjZWQgYnkgZnVuY3Rpb24gbmFtZSBpZiB0aGF0IG5hbWUgaXMgZGlmZmVyZW50IHRvIHBhdGNoXHJcbiAqIEBwYXJhbSBtaWRkbGV3YXJlcyBhbGwgbWlkZGxld2FyZSBmdW5jdGlvbnMgZm9yIHRoZSByZXF1ZXN0XHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gSHR0cFBBVENIKHJvdXRlPzogc3RyaW5nLCBtaWRkbGV3YXJlcz86IEFycmF5PChyZXF1ZXN0PywgcmVzcG9uc2U/LCBuZXh0PykgPT4gdm9pZD4pIHtcclxuICAgIHJldHVybiBmdW5jdGlvbiAodGFyZ2V0LCBwcm9wZXJ0eU5hbWUpIHtcclxuICAgICAgICBpbml0KHRhcmdldCwgXCJfX2ZuUGF0Y2hcIik7XHJcbiAgICAgICAgYWRkRnVuY3Rpb24odGFyZ2V0Ll9fZm5QYXRjaCwgdGFyZ2V0LCBwcm9wZXJ0eU5hbWUsIG1pZGRsZXdhcmVzLCByb3V0ZSk7XHJcbiAgICB9XHJcbn1cclxuLyoqXHJcbiAqIHRoYXQgc2V0IGEgQ29udHJvbGxlciBmdW5jdGlvbiBhcyBhIHNpbXBsZSBIdHRwIEhFQUQgcmVxdWVzdFxyXG4gKiBAcGFyYW0gcm91dGUgbmFtZSBvZiB0aGUgSHR0cCBIRUFEIHJlcXVlc3QsIGlmIG51bGwgd2lsbCBiZSBhdXRvbWF0aWNhbGx5IHJlcGxhY2VkIGJ5IGZ1bmN0aW9uIG5hbWUgaWYgdGhhdCBuYW1lIGlzIGRpZmZlcmVudCB0byBoZWFkXHJcbiAqIEBwYXJhbSBtaWRkbGV3YXJlcyBhbGwgbWlkZGxld2FyZSBmdW5jdGlvbnMgZm9yIHRoZSByZXF1ZXN0XHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gSHR0cEhFQUQocm91dGU/OiBzdHJpbmcsIG1pZGRsZXdhcmVzPzogQXJyYXk8KHJlcXVlc3Q/LCByZXNwb25zZT8sIG5leHQ/KSA9PiB2b2lkPikge1xyXG4gICAgcmV0dXJuIGZ1bmN0aW9uICh0YXJnZXQsIHByb3BlcnR5TmFtZSkge1xyXG4gICAgICAgIGluaXQodGFyZ2V0LCBcIl9fZm5IZWFkXCIpO1xyXG4gICAgICAgIGFkZEZ1bmN0aW9uKHRhcmdldC5fX2ZuSGVhZCwgdGFyZ2V0LCBwcm9wZXJ0eU5hbWUsIG1pZGRsZXdhcmVzLCByb3V0ZSk7XHJcbiAgICB9XHJcbn1cclxuLyoqXHJcbiAqIHRoYXQgc2V0IGEgQ29udHJvbGxlciBmdW5jdGlvbiBhcyBhIHNpbXBsZSBIdHRwIENPUFkgcmVxdWVzdFxyXG4gKiBAcGFyYW0gcm91dGUgbmFtZSBvZiB0aGUgSHR0cCBDT1BZIHJlcXVlc3QsIGlmIG51bGwgd2lsbCBiZSBhdXRvbWF0aWNhbGx5IHJlcGxhY2VkIGJ5IGZ1bmN0aW9uIG5hbWUgaWYgdGhhdCBuYW1lIGlzIGRpZmZlcmVudCB0byBjb3B5XHJcbiAqIEBwYXJhbSBtaWRkbGV3YXJlcyBhbGwgbWlkZGxld2FyZSBmdW5jdGlvbnMgZm9yIHRoZSByZXF1ZXN0XHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gSHR0cENPUFkocm91dGU/OiBzdHJpbmcsIG1pZGRsZXdhcmVzPzogQXJyYXk8KHJlcXVlc3Q/LCByZXNwb25zZT8sIG5leHQ/KSA9PiB2b2lkPikge1xyXG4gICAgcmV0dXJuIGZ1bmN0aW9uICh0YXJnZXQsIHByb3BlcnR5TmFtZSkge1xyXG4gICAgICAgIGluaXQodGFyZ2V0LCBcIl9fZm5Db3B5XCIpO1xyXG4gICAgICAgIGFkZEZ1bmN0aW9uKHRhcmdldC5fX2ZuQ29weSwgdGFyZ2V0LCBwcm9wZXJ0eU5hbWUsIG1pZGRsZXdhcmVzLCByb3V0ZSk7XHJcbiAgICB9XHJcbn1cclxuLyoqXHJcbiAqIHRoYXQgc2V0IGEgQ29udHJvbGxlciBmdW5jdGlvbiBhcyBhIHNpbXBsZSBIdHRwIExJTksgcmVxdWVzdFxyXG4gKiBAcGFyYW0gcm91dGUgbmFtZSBvZiB0aGUgSHR0cCBMSU5LIHJlcXVlc3QsIGlmIG51bGwgd2lsbCBiZSBhdXRvbWF0aWNhbGx5IHJlcGxhY2VkIGJ5IGZ1bmN0aW9uIG5hbWUgaWYgdGhhdCBuYW1lIGlzIGRpZmZlcmVudCB0byBsaW5rXHJcbiAqIEBwYXJhbSBtaWRkbGV3YXJlcyBhbGwgbWlkZGxld2FyZSBmdW5jdGlvbnMgZm9yIHRoZSByZXF1ZXN0XHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gSHR0cExJTksocm91dGU/OiBzdHJpbmcsIG1pZGRsZXdhcmVzPzogQXJyYXk8KHJlcXVlc3Q/LCByZXNwb25zZT8sIG5leHQ/KSA9PiB2b2lkPikge1xyXG4gICAgcmV0dXJuIGZ1bmN0aW9uICh0YXJnZXQsIHByb3BlcnR5TmFtZSkge1xyXG4gICAgICAgIGluaXQodGFyZ2V0LCBcIl9fZm5MaW5rXCIpO1xyXG4gICAgICAgIGFkZEZ1bmN0aW9uKHRhcmdldC5fX2ZuTGluaywgdGFyZ2V0LCBwcm9wZXJ0eU5hbWUsIG1pZGRsZXdhcmVzLCByb3V0ZSk7XHJcbiAgICB9XHJcbn1cclxuLyoqXHJcbiAqIHRoYXQgc2V0IGEgQ29udHJvbGxlciBmdW5jdGlvbiBhcyBhIHNpbXBsZSBIdHRwIFVOTElOSyByZXF1ZXN0XHJcbiAqIEBwYXJhbSByb3V0ZSBuYW1lIG9mIHRoZSBIdHRwIFVOTElOSyByZXF1ZXN0LCBpZiBudWxsIHdpbGwgYmUgYXV0b21hdGljYWxseSByZXBsYWNlZCBieSBmdW5jdGlvbiBuYW1lIGlmIHRoYXQgbmFtZSBpcyBkaWZmZXJlbnQgdG8gdW5saW5rXHJcbiAqIEBwYXJhbSBtaWRkbGV3YXJlcyBhbGwgbWlkZGxld2FyZSBmdW5jdGlvbnMgZm9yIHRoZSByZXF1ZXN0XHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gSHR0cFVOTElOSyhyb3V0ZT86IHN0cmluZywgbWlkZGxld2FyZXM/OiBBcnJheTwocmVxdWVzdD8sIHJlc3BvbnNlPywgbmV4dD8pID0+IHZvaWQ+KSB7XHJcbiAgICByZXR1cm4gZnVuY3Rpb24gKHRhcmdldCwgcHJvcGVydHlOYW1lKSB7XHJcbiAgICAgICAgaW5pdCh0YXJnZXQsIFwiX19mblVubGlua1wiKTtcclxuICAgICAgICBhZGRGdW5jdGlvbih0YXJnZXQuX19mblVubGluaywgdGFyZ2V0LCBwcm9wZXJ0eU5hbWUsIG1pZGRsZXdhcmVzLCByb3V0ZSk7XHJcbiAgICB9XHJcbn1cclxuLyoqXHJcbiAqIHRoYXQgc2V0IGEgQ29udHJvbGxlciBmdW5jdGlvbiBhcyBhIHNpbXBsZSBIdHRwIFBVUkdFIHJlcXVlc3RcclxuICogQHBhcmFtIHJvdXRlIG5hbWUgb2YgdGhlIEh0dHAgUFVSR0UgcmVxdWVzdCwgaWYgbnVsbCB3aWxsIGJlIGF1dG9tYXRpY2FsbHkgcmVwbGFjZWQgYnkgZnVuY3Rpb24gbmFtZSBpZiB0aGF0IG5hbWUgaXMgZGlmZmVyZW50IHRvIHB1cmdlXHJcbiAqIEBwYXJhbSBtaWRkbGV3YXJlcyBhbGwgbWlkZGxld2FyZSBmdW5jdGlvbnMgZm9yIHRoZSByZXF1ZXN0XHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gSHR0cFBVUkdFKHJvdXRlPzogc3RyaW5nLCBtaWRkbGV3YXJlcz86IEFycmF5PChyZXF1ZXN0PywgcmVzcG9uc2U/LCBuZXh0PykgPT4gdm9pZD4pIHtcclxuICAgIHJldHVybiBmdW5jdGlvbiAodGFyZ2V0LCBwcm9wZXJ0eU5hbWUpIHtcclxuICAgICAgICBpbml0KHRhcmdldCwgXCJfX2ZuUHVyZ2VcIik7XHJcbiAgICAgICAgYWRkRnVuY3Rpb24odGFyZ2V0Ll9fZm5QdXJnZSwgdGFyZ2V0LCBwcm9wZXJ0eU5hbWUsIG1pZGRsZXdhcmVzLCByb3V0ZSk7XHJcbiAgICB9XHJcbn1cclxuLyoqXHJcbiAqIHRoYXQgc2V0IGEgQ29udHJvbGxlciBmdW5jdGlvbiBhcyBhIHNpbXBsZSBIdHRwIFBST1BGSU5EIHJlcXVlc3RcclxuICogQHBhcmFtIHJvdXRlIG5hbWUgb2YgdGhlIEh0dHAgUFJPUEZJTkQgcmVxdWVzdCwgaWYgbnVsbCB3aWxsIGJlIGF1dG9tYXRpY2FsbHkgcmVwbGFjZWQgYnkgZnVuY3Rpb24gbmFtZSBpZiB0aGF0IG5hbWUgaXMgZGlmZmVyZW50IHRvIHByb3BmaW5kXHJcbiAqIEBwYXJhbSBtaWRkbGV3YXJlcyBhbGwgbWlkZGxld2FyZSBmdW5jdGlvbnMgZm9yIHRoZSByZXF1ZXN0XHJcbiAqL1xyXG5leHBvcnQgZnVuY3Rpb24gSHR0cFBST1BGSU5EKHJvdXRlPzogc3RyaW5nLCBtaWRkbGV3YXJlcz86IEFycmF5PChyZXF1ZXN0PywgcmVzcG9uc2U/LCBuZXh0PykgPT4gdm9pZD4pIHtcclxuICAgIHJldHVybiBmdW5jdGlvbiAodGFyZ2V0LCBwcm9wZXJ0eU5hbWUpIHtcclxuICAgICAgICBpbml0KHRhcmdldCwgXCJfX2ZuUHJvcGZpbmRcIik7XHJcbiAgICAgICAgYWRkRnVuY3Rpb24odGFyZ2V0Ll9fZm5Qcm9wZmluZCwgdGFyZ2V0LCBwcm9wZXJ0eU5hbWUsIG1pZGRsZXdhcmVzLCByb3V0ZSk7XHJcbiAgICB9XHJcbn0iXX0=