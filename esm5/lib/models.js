/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*! *****************************************************************************
    Copyright (c) Di Sante Simone. All rights reserved.
    
    This file is part of lib webapinode.

    webapinode is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    webapinode is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    For GNU General Public License see <http://www.gnu.org/licenses/>.
*/
var fnObject = /** @class */ (function () {
    function fnObject() {
        this.props = { route: "", propertyName: "", parsed: false };
    }
    return fnObject;
}());
export { fnObject };
if (false) {
    /** @type {?} */
    fnObject.prototype.fn;
    /** @type {?} */
    fnObject.prototype.middlewares;
    /** @type {?} */
    fnObject.prototype.props;
}
/** @enum {number} */
var callType = {
    GET: 0,
    POST: 1,
    PUT: 2,
    DELETE: 3,
    PATCH: 4,
    HEAD: 5,
    COPY: 6,
    LINK: 7,
    UNLINK: 8,
    PURGE: 9,
    PROPFIND: 10,
    LOCK: 11,
    UNLOCK: 12,
};
export { callType };
callType[callType.GET] = 'GET';
callType[callType.POST] = 'POST';
callType[callType.PUT] = 'PUT';
callType[callType.DELETE] = 'DELETE';
callType[callType.PATCH] = 'PATCH';
callType[callType.HEAD] = 'HEAD';
callType[callType.COPY] = 'COPY';
callType[callType.LINK] = 'LINK';
callType[callType.UNLINK] = 'UNLINK';
callType[callType.PURGE] = 'PURGE';
callType[callType.PROPFIND] = 'PROPFIND';
callType[callType.LOCK] = 'LOCK';
callType[callType.UNLOCK] = 'UNLOCK';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9kZWxzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vd2ViYXBpbW9kZWwvIiwic291cmNlcyI6WyJsaWIvbW9kZWxzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQTtJQUFBO1FBR0ksVUFBSyxHQUE2RCxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsWUFBWSxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLENBQUE7SUFDcEgsQ0FBQztJQUFELGVBQUM7QUFBRCxDQUFDLEFBSkQsSUFJQzs7OztJQUhHLHNCQUFHOztJQUNILCtCQUFZOztJQUNaLHlCQUFnSDs7OztJQUloSCxNQUFHO0lBQ0gsT0FBSTtJQUNKLE1BQUc7SUFDSCxTQUFNO0lBQ04sUUFBSztJQUNMLE9BQUk7SUFDSixPQUFJO0lBQ0osT0FBSTtJQUNKLFNBQU07SUFDTixRQUFLO0lBQ0wsWUFBUTtJQUNSLFFBQUk7SUFDSixVQUFNIiwic291cmNlc0NvbnRlbnQiOlsiIFxyXG4vKiEgKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKioqKipcclxuICAgIENvcHlyaWdodCAoYykgRGkgU2FudGUgU2ltb25lLiBBbGwgcmlnaHRzIHJlc2VydmVkLlxyXG4gICAgXHJcbiAgICBUaGlzIGZpbGUgaXMgcGFydCBvZiBsaWIgd2ViYXBpbm9kZS5cclxuXHJcbiAgICB3ZWJhcGlub2RlIGlzIGZyZWUgc29mdHdhcmU6IHlvdSBjYW4gcmVkaXN0cmlidXRlIGl0IGFuZC9vciBtb2RpZnlcclxuICAgIGl0IHVuZGVyIHRoZSB0ZXJtcyBvZiB0aGUgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgYXMgcHVibGlzaGVkIGJ5XHJcbiAgICB0aGUgRnJlZSBTb2Z0d2FyZSBGb3VuZGF0aW9uLCBlaXRoZXIgdmVyc2lvbiAzIG9mIHRoZSBMaWNlbnNlLCBvclxyXG4gICAgKGF0IHlvdXIgb3B0aW9uKSBhbnkgbGF0ZXIgdmVyc2lvbi5cclxuXHJcbiAgICB3ZWJhcGlub2RlIGlzIGRpc3RyaWJ1dGVkIGluIHRoZSBob3BlIHRoYXQgaXQgd2lsbCBiZSB1c2VmdWwsXHJcbiAgICBidXQgV0lUSE9VVCBBTlkgV0FSUkFOVFk7IHdpdGhvdXQgZXZlbiB0aGUgaW1wbGllZCB3YXJyYW50eSBvZlxyXG4gICAgTUVSQ0hBTlRBQklMSVRZIG9yIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFLiAgU2VlIHRoZVxyXG4gICAgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2UgZm9yIG1vcmUgZGV0YWlscy5cclxuXHJcbiAgICBGb3IgR05VIEdlbmVyYWwgUHVibGljIExpY2Vuc2Ugc2VlIDxodHRwOi8vd3d3LmdudS5vcmcvbGljZW5zZXMvPi5cclxuKi9cclxuXHJcbmV4cG9ydCBjbGFzcyBmbk9iamVjdCB7XHJcbiAgICBmbjtcclxuICAgIG1pZGRsZXdhcmVzO1xyXG4gICAgcHJvcHM6IHsgcm91dGU6IHN0cmluZywgcHJvcGVydHlOYW1lOiBzdHJpbmcsIHBhcnNlZDogYm9vbGVhbiB9ID0geyByb3V0ZTogXCJcIiwgcHJvcGVydHlOYW1lOiBcIlwiLCBwYXJzZWQ6IGZhbHNlIH1cclxufVxyXG5cclxuZXhwb3J0IGVudW0gY2FsbFR5cGUge1xyXG4gICAgR0VULFxyXG4gICAgUE9TVCxcclxuICAgIFBVVCxcclxuICAgIERFTEVURSxcclxuICAgIFBBVENILFxyXG4gICAgSEVBRCxcclxuICAgIENPUFksXHJcbiAgICBMSU5LLFxyXG4gICAgVU5MSU5LLFxyXG4gICAgUFVSR0UsXHJcbiAgICBQUk9QRklORCxcclxuICAgIExPQ0ssXHJcbiAgICBVTkxPQ0tcclxufSJdfQ==